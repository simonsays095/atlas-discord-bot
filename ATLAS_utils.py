import numpy as np
import os
from dotenv import load_dotenv

load_dotenv()

CLUSTER_ID = 1
ASK_USER_ID = os.getenv("ASK_USER_ID")
assert ASK_USER_ID is not None, "Please include an \"ASK_USER_ID\" environment variable."
ASK_USER_ID = int(ASK_USER_ID)


def CoordToID(coord):
    '''
    Converts a coordinate (A-O followed by 1-15) into an ID used for the API.
    '''
    row = coord[0]
    col = int(coord[1:])
    print("unpacking coordinate: row=", row, "and col=", col)
    if not (ord('A') <= ord(row) <= ord('O') and 1 <= col <= 15):
        raise ValueError("Coordinate is invalid.")
    return 15 * (ord(row) - ord('A')) + col + 225 * (CLUSTER_ID - 1)


async def PrintTo(ctx, message, delete_after=None, raw=False, file=None, embed=None, maxLen=1990):
    '''
    Simple wrapper to the ctx.send() function, which formats the message for discord.

    Note that this function will not check for a maximum text width, you have to use
    GetMatPrintString() for that functionality.
    '''
    subMessages = []
    while message is not None:
        message = str(message)  # Ensure message is a string
        if len(message) > maxLen:
            idx = message.rfind('\n', 0, maxLen)
            subMessages.append(message[:idx])
            message = message[idx + 1:]
        else:
            subMessages.append(message)
            message = None
    messageList = []
    for sub in subMessages:
        if raw:
            newMessage = await ctx.send(sub, file=file, embed=embed)
        else:
            newMessage = await ctx.send("```" + sub + "```", file=file, embed=embed)
        messageList.append(newMessage)
    if len(messageList) == 1:
        messageList = messageList[0]
    return messageList


def GetMatPrintString(matrix, MAX_WIDTH=145):
    '''
    Generates a string representation of a data matrix, that is formatted to look nice in discord.

    Works with both lists and matrices. Lists are represented in a row.
    '''
    matrix = np.asarray(matrix)  # Make sure the matrix is a numpy array, not a list
    message = ""
    if matrix.ndim == 1:
        matrix = np.reshape(matrix, (1, -1))  # Make a list into a row in a matrix.
    numCols = matrix.shape[1]
    colWidths = np.array([0] * numCols)  # width of each column

    colGroups = [[]]  # which columns can be put side by side with good formatting

    for i in range(numCols):
        colWidths[i] = max([len(str(row[i])) for row in matrix]) + 2
        if np.sum(colWidths[colGroups[-1]]) + colWidths[i] > MAX_WIDTH:
            colGroups.append([i])
        else:
            colGroups[-1].append(i)

    for cols in colGroups:
        for row in matrix:
            for i, col in enumerate(np.array(row)[cols]):
                message += str(col).ljust(colWidths[cols[i]])
            message += "\n"
        message += "\n"
    return message


def is_approved_responder(ctx):
    '''
    Predicate function to check if the user who sent the message is approved to use admin-level commands.
    '''
    approved = [ASK_USER_ID]
    return ctx.message.author.id in approved


class Message:
    def __init__(self, ctx, message=None):
        self.ctx = ctx
        self.message = message
        self.raw = False
        self.file = None
        self.embed = None
        self.delete_after = None

    async def Print(self):
        if self.message == "":
            self.message = None
        await PrintTo(self.ctx, self.message, self.delete_after, self.raw, self.file, self.embed)