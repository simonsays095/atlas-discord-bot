import sqlite3, os, discord, asyncio
import numpy as np
from discord.ext import commands
from dotenv import load_dotenv
from shutil import copyfile
from enum import Enum

def dict_factory(cursor, row):
    '''
    Used in sql tables to extract data into a dict.

    Source: https://docs.python.org/3/library/sqlite3.html
    '''
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

DATABASE_FILE = "discord_shop.db"

conn = sqlite3.connect(DATABASE_FILE)

conn.row_factory = dict_factory
c = conn.cursor()
c.execute("SELECT name FROM sqlite_master WHERE type='table';")
print("Printing all tables:")
tables = c.fetchall()
print(tables)

values = {
    'number' : str,
    'category_id' : int,
    'listing_id' : int,
    'sex' : str,
    'price' : int,
    'level' : int,
    'notes' : str,
    'sale_state' : str,
    'seller' : int,
    'breedable' : int,
    'reserver_id' : int,
    'reserve_staff_id' : int,
    'reserve_date' : str,
    'buyer_id' : int,
    'seller_staff_id' : int,
    'sold_date' : str,
    'identifier' : str,
    'breed' : str,
    'guild_id' : int
}

# Figure out if we need a suffix number for backup file.
suffix = 0
backupFile = DATABASE_FILE + ".bak" + str(suffix)
while os.path.exists(backupFile):
    suffix += 1
    backupFile = DATABASE_FILE + ".bak" + str(suffix)
print("Saving backup database to " + backupFile)

copyfile(DATABASE_FILE, backupFile)

c.execute("SELECT * FROM \
        animal INNER JOIN category ON animal.category_id = category.id \
               INNER JOIN whitelisted_guild ON category.guild_id = whitelisted_guild.id")
rows = c.fetchall()
for row in rows:
    newData = {}
    for key in row:
        if row[key] is None or key not in values: continue
        if type(row[key]) != values[key]:
            print(row)
            val = input("Enter new value for " + key + " (or press enter to leave it):")
            if val == "": continue
            print(row['identifier'] + row['number'] + " has had its " + key + " changed from " + row[key] + " to " + val)
            newData[key] = val
    if newData:
        c.execute("UPDATE animal SET " + ', '.join([key + " = " + str(value) for key, value in newData.items()]) \
            + " WHERE number = ? AND category_id = ?", (row['number'], row['category_id']))
        if c.rowcount: print("Updated properly")
        conn.commit()

c.execute("SELECT * FROM sold INNER JOIN whitelisted_guild ON sold.guild_id = whitelisted_guild.id")
rows = c.fetchall()
for row in rows:
    newData = {}
    for key in row:
        if row[key] is None or key not in values: continue
        if type(row[key]) != values[key]:
            print(row)
            val = input("Enter new value for " + key + " (or press enter to leave it):")
            if val == "": continue
            print(row['identifier'] + row['number'] + " has had its " + key + " changed from " + row[key] + " to " + val)
            newData[key] = val
    if newData:
        # Not a perfect query, but only fails if multiple animals with the same code are sold in the same day on the same server
        c.execute("UPDATE sold SET " + ', '.join([key + " = " + str(value) for key, value in newData.items()]) \
            + " WHERE number = ? AND identifier = ? AND sold_date = ? AND guild_id = ?", (row['number'], row['identifier'], row['sold_date'], row['guild_id']))
        if c.rowcount: print("Updated properly")
        conn.commit()

animals = c.execute("SELECT * FROM animal").fetchall()
for animal in animals:
    print(animal)
