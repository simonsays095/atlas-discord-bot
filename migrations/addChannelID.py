import sqlite3, os, discord, asyncio
import numpy as np
from discord.ext import commands
from dotenv import load_dotenv
from shutil import copyfile
from enum import Enum

# Quick-fix script. Backs up and then changes the database structure in the following way:
#
#   - Adds the channel_id column to the sale_items table
#   - Will go through any listings with a listing_id and add the appropriate channel_id to that listing
#   - If any listings still have a listing_id when the message is deleted, it will set listing_id to NULL
#   - Previously a table called "Channels" was accidentally created - will kill that if it exists.

DATABASE_FILE = "discord_shop.db"

def dict_factory(cursor, row):
    '''
    Used in sql tables to extract data into a dict.

    Source: https://docs.python.org/3/library/sqlite3.html
    '''
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

conn = sqlite3.connect(DATABASE_FILE)
conn.row_factory = dict_factory
c = conn.cursor()
c.execute("SELECT name FROM sqlite_master WHERE type='table';")
print("Printing all tables:")
print(c.fetchall())
c.execute("DROP TABLE IF EXISTS Channels")
conn.commit()


# Figure out if we need a suffix number for backup file.
suffix = 0
backupFile = DATABASE_FILE + ".bak" + str(suffix)
while os.path.exists(backupFile):
    suffix += 1
    backupFile = DATABASE_FILE + ".bak" + str(suffix)
print("Saving backup database to " + backupFile)

copyfile(DATABASE_FILE, backupFile)

# Change the format of sale_items
c.execute("ALTER TABLE sale_items ADD COLUMN channel_id INTEGER DEFAULT NULL")
conn.commit()

# Look through listings and add appropriate channel_ids.
load_dotenv()
print(".env loaded")
DISCORD_BOT_TOKEN = os.getenv("DISCORD_BOT_TOKEN")
bot = commands.Bot(command_prefix="!", case_insensitive=True)
print("Bot created")

@bot.event
async def on_ready(): # Log on, do what we need, log off

    entries = c.execute("SELECT listing_id FROM sale_items WHERE listing_id IS NOT NULL").fetchall()
    channels = c.execute("SELECT * FROM shop_channels").fetchall()
    print(channels)
    for entry in entries:
        listing_id = entry["listing_id"]
        print("listing id:", listing_id)
        message = None
        for channel in channels:
            channel = bot.get_channel(channel["channel_id"])
            try:
                message = await channel.fetch_message(listing_id)
            except discord.NotFound:
                print("\tchannel id: Nope")
                continue # listing not in this channel
            # A message was found
            print("\tchannel id: Yep")
            c.execute("UPDATE sale_items SET channel_id = ? WHERE listing_id = ?", (channel.id, listing_id))
            break
        if not message:
            # No message was found in any channel
            print("Could not find a message associated with this listing:", listing_id)
            c.execute("UPDATE sale_items SET listing_id = NULL WHERE listing_id = ?", (listing_id,))
    conn.commit()

    await bot.logout()

bot.run(DISCORD_BOT_TOKEN)
print("Bot now off")

c.execute("SELECT * FROM sale_items")
sales = c.fetchall()
print("New contents:")
mat = [["sale_id", "listing id", "channel id"]]
for sale in sales:
    mat += [[sale["sale_id"],sale["listing_id"],sale["channel_id"]]]
print(mat)
