import sqlite3, os, discord, asyncio
import numpy as np
from discord.ext import commands
from dotenv import load_dotenv
from shutil import copyfile
from enum import Enum
from ATLAS_utils import GetMatPrintString

# Quick-fix script. Backs up and then changes the database structure in the following way:
#
#   - Changes the 'date' column to a 'date_reserved' and a 'date_sold' column separately
#   - Changes the 'staff' column to a 'staff_reserved' and a 'staff_sold' column separately
#   - Makes all columns snake_case rather than UpperCamelCase
#   - Adds in the price, listing_id, item_type, and level columns
#   - Changes the disc_factory so that sale_state can be interpreted as a new enum
#
#     For the date and staff columns, it will assume that sold items are reserved/sold on the same day by
# same person. Also, if items are reserved but not sold yet, it will keep track of that.
#
#     Lastly, it adds the table "shop_members" to the database, which keeps track of display names of people
# That use the shop. This is so we can remember who bought specific items from us, even if they left the server.

class State(Enum):
    UNKNOWN = -1
    FOR_SALE = 0
    RESERVED = 1
    SOLD = 2

    def __str__(self):
        return self.name

    def __int__(self):
        return self.value

def dict_factory(cursor, row):
    '''
    Used in sql tables to extract data into a dict.

    Source: https://docs.python.org/3/library/sqlite3.html
    '''
    d = {}
    for idx, col in enumerate(cursor.description):
        if col[0] == "sale_state": # Convert string into enum
            d[col[0]] = getattr(State, row[idx], "UNKNOWN")
        else:
            d[col[0]] = row[idx]
    return d

DATABASE_FILE = "discord_shop.db"

# Open up the bot, so we can get username information
load_dotenv()
print(".env loaded")
DISCORD_BOT_TOKEN = os.getenv("DISCORD_BOT_TOKEN")
bot = commands.Bot(command_prefix="!", case_insensitive=True)
print("Bot created")

@bot.event
async def on_ready(): # Just log on and log back off
    await bot.logout()

bot.run(DISCORD_BOT_TOKEN)
print("Bot now off")

conn = sqlite3.connect(DATABASE_FILE)
conn.row_factory = dict_factory
c = conn.cursor()

# Figure out if we need a suffix number for backup file.
suffix = 0
backupFile = DATABASE_FILE + ".bak" + str(suffix)
while os.path.exists(backupFile):
    suffix += 1
    backupFile = DATABASE_FILE + ".bak" + str(suffix)
print("Saving backup database to " + backupFile)

copyfile(DATABASE_FILE, backupFile)

# Create tables with the new formats
c.execute("CREATE TABLE IF NOT EXISTS \
        sale_items (id INTEGER PRIMARY KEY, \
                sale_id TEXT NOT NULL, \
                item_type TEXT DEFAULT NULL, \
                price INTEGER DEFAULT NULL, \
                listing_id INTEGER DEFAULT NULL, \
                level INTEGER DEFAULT NULL, \
                sale_state TEXT NOT NULL, \
                buyer INTEGER DEFAULT NULL, \
                staff_reserved INTEGER DEFAULT NULL, \
                date_reserved TEXT DEFAULT NULL, \
                staff_sold INTEGER DEFAULT NULL, \
                date_sold TEXT DEFAULT NULL);")
c.execute("CREATE TABLE IF NOT EXISTS \
        shop_members (discord_id INTEGER PRIMARY KEY, \
                display_name TEXT NOT NULL)")
conn.commit()

# Gather data
c.execute("SELECT * FROM SaleItems")
sales = c.fetchall()
print("Old contents:")
mat = [["name", "saleState", "buyer", "staff", "date"]]
for sale in sales:
    mat += [[sale["name"], sale["saleState"],sale["buyer"],sale["staff"],sale["date"]]]
print(GetMatPrintString(mat))

# Convert relevant values - only buyer is not changed.
for sale in sales:
    sale["sale_id"] = sale["name"]

    if sale["saleState"] == 2: # SOLD
        sale["sale_state"] = str(State.SOLD)
        sale["staff_sold"] = sale["staff"]
        sale["staff_reserved"] = sale["staff"] # Assume the seller also reserved it.
        sale["date_sold"] = sale["date"]
        sale["date_reserved"] = sale["date"] # Assume purchased on the same day - probably not true
    elif sale["saleState"] == 1: # Reserved
        sale["sale_state"] = str(State.RESERVED)
        sale["staff_sold"] = None
        sale["staff_reserved"] = sale["staff"]
        sale["date_sold"] = None
        sale["date_reserved"] = sale["date"]
    elif sale["saleState"] == 0: # for sale still
        sale["sale_state"] = str(State.FOR_SALE)
        sale["staff_sold"] = None
        sale["staff_reserved"] = None
        sale["date_sold"] = None
        sale["date_reserved"] = None
    else: # Unknown sale state
        print("Not sure about this sale's state... Assuming it's for sale?")
        print(sale["name"], sale["saleState"])
        sale["sale_state"] = str(State.UNKNOWN)
        sale["staff_sold"] = None
        sale["staff_reserved"] = None
        sale["date_sold"] = None
        sale["date_reserved"] = None

    # Insert this data into the new table. Don't insert price, item_type, or listing_id since that data doesn't exist
    c.execute("INSERT INTO sale_items (sale_id, sale_state, buyer, staff_reserved, date_reserved, staff_sold, date_sold) \
            VALUES (?, ?, ?, ?, ?, ?, ?)", (sale["sale_id"],sale["sale_state"],sale["buyer"],sale["staff_reserved"],sale["date_reserved"],sale["staff_sold"],sale["date_sold"]))
conn.commit()

c.execute("SELECT * FROM sale_items")
sales = c.fetchall()
print("New contents:")
mat = [["sale_id", "item_type", "price", "level", "sale_state", "buyer", "staff_reserved", "date_reserved", "staff_sold", "date_sold"]]
for sale in sales:
    mat += [[sale["sale_id"],sale["item_type"],sale["price"],sale["level"],sale["sale_state"],sale["buyer"],sale["staff_reserved"],sale["date_reserved"],sale["staff_sold"],sale["date_sold"]]]
print(GetMatPrintString(mat))

c.execute("DROP TABLE SaleItems")

# Now you need to go into the bot and populate the shop_members
# Extract all relevant data from sale_items table
c.execute("SELECT buyer, staff_reserved, staff_sold FROM sale_items")
sales = c.fetchall()

# For each buyer and staff member (reserving or selling) get their current name, if possible.
membersList = list(bot.get_all_members())
for sale in sales:
    buyer = sr = ss = None
    for member in membersList:
        if sale['buyer'] == member.id: buyer = member
        if sale['staff_reserved'] == member.id: sr = member
        if sale['staff_sold'] == member.id: ss = member

    # Check for unknown members
    buyerName = buyer.display_name if buyer else "Unknown"
    srName = sr.display_name if sr else "Unknown"
    ssName = ss.display_name if ss else "Unknown"
    c.execute("INSERT OR IGNORE INTO shop_members (discord_id, display_name) VALUES (?, ?)", (sale['buyer'], buyerName))
    if sale['staff_reserved']:
        c.execute("INSERT OR IGNORE INTO shop_members (discord_id, display_name) VALUES (?, ?)", (sale['staff_reserved'], srName))
    if sale['staff_sold']:
        c.execute("INSERT OR IGNORE INTO shop_members (discord_id, display_name) VALUES (?, ?)", (sale['staff_sold'], ssName))
conn.commit()

c.execute("SELECT * FROM shop_members")
members = c.fetchall()
mat = [["Discord ID", "Display Name"]]
for member in members:
    mat += [[member["discord_id"], member["display_name"]]]
print(GetMatPrintString(mat))
