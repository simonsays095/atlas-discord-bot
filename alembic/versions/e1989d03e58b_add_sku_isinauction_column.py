"""Add sku.IsInAuction column

Revision ID: e1989d03e58b
Revises: 
Create Date: 2020-08-23 00:05:14.747854

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e1989d03e58b'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('sku', sa.Column('IsInAuction', sa.Boolean))
    op.add_column('sku_version', sa.Column('IsInAuction', sa.Boolean))
    op.execute("UPDATE sku SET IsInAuction = false")
    op.execute("UPDATE sku_version SET IsInAuction = false")


def downgrade():
    op.drop_column('sku', 'IsInAuction')
    op.drop_column('sku_version', 'IsInAuction')
