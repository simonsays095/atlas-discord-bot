import sqlite3
import os
from contextlib import contextmanager
from enum import Enum
from cogs.cog_utils.shop_schema import Base
from dotenv import load_dotenv
from datetime import datetime, timedelta
from sqlalchemy.orm import sessionmaker
# import sqlalchemy
from sqlalchemy import create_engine
from cogs.cog_utils.shop_schema import Guild, Member, Channel, Type, Animal, Note
from cogs.cog_utils.shop_schema import SaleType, SaleState  # , Transactions  # Enums
from cogs.cog_utils.shop_schema import Posting, Reservation  # , UnReservation, Finalization, Cancellation  # , Transaction


load_dotenv()


DATABASE_FILE = "discord_shop.db"
DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')
DB_HOST = os.getenv('DB_HOST')
print(DB_USER, DB_PASS, DB_HOST)


@contextmanager
def session_scope(db, query=False):
    """Provide a transactional scope around a series of operations."""
    session = db()
    try:
        yield session
        if not query:
            session.commit()
    except Exception as e:
        session.rollback()
        print(e)
        raise e
    finally:
        session.close()


class State(Enum):
    UNKNOWN = -1
    FOR_SALE = 0
    RESERVED = 1
    SOLD = 2

    def __str__(self):
        return self.name


def dict_factory(cursor, row):
    '''
    Used in sql tables to extract data into a dict.

    Source: https://docs.python.org/3/library/sqlite3.html
    '''
    d = {}
    for idx, col in enumerate(cursor.description):
        if col[0] == "sale_state":  # Convert string into enum
            d[col[0]] = getattr(State, row[idx], "UNKNOWN")
        else:
            d[col[0]] = row[idx]
    return d


# Load sqlite db like usual
conn = sqlite3.connect(DATABASE_FILE)
conn.row_factory = dict_factory
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS \
        whitelisted_guild (id INTEGER PRIMARY KEY, \
                backend_channel_id INTEGER DEFAULT NULL, \
                shop_category_id INTEGER DEFAULT NULL, \
                accepted_role_id INTEGER DEFAULT NULL)")
c.execute("CREATE TABLE IF NOT EXISTS \
        member (discord_id INTEGER PRIMARY KEY, \
                display_name TEXT NOT NULL)")
c.execute("CREATE TABLE IF NOT EXISTS \
        guild_member (id INTEGER PRIMARY KEY NOT NULL, \
                discord_id INTEGER REFERENCES member(discord_id), \
                guild_id INTEGER REFERENCES whitelisted_guild(id), \
            UNIQUE(discord_id, guild_id))")
c.execute("CREATE TABLE IF NOT EXISTS \
        category (id INTEGER PRIMARY KEY, \
                channel_id INTEGER NOT NULL, \
                guild_id INTEGER REFERENCES whitelisted_guild(id) \
                    ON UPDATE CASCADE ON DELETE CASCADE, \
                breed TEXT NOT NULL, \
                identifier TEXT NOT NULL, \
            UNIQUE(guild_id, identifier))")
c.execute("CREATE TABLE IF NOT EXISTS \
        animal (number TEXT NOT NULL, \
                category_id INTEGER NOT NULL REFERENCES category(id) \
                    ON UPDATE CASCADE ON DELETE CASCADE, \
                listing_id INTEGER DEFAULT NULL, \
                sex TEXT DEFAULT NULL, \
                price INTEGER DEFAULT NULL, \
                level INTEGER DEFAULT NULL, \
                notes TEXT DEFAULT NULL, \
                sale_state TEXT NOT NULL, \
                seller INTEGER NOT NULL, \
                breedable INTEGER DEFAULT 0, \
                reserver_id INTEGER DEFAULT NULL REFERENCES member(discord_id) \
                    ON UPDATE CASCADE ON DELETE CASCADE, \
                reserve_staff_id INTEGER DEFAULT NULL REFERENCES member(discord_id) \
                    ON UPDATE CASCADE ON DELETE CASCADE, \
                reserve_date TEXT DEFAULT NULL, \
                buyer_id INTEGER DEFAULT NULL REFERENCES member(discord_id) \
                    ON UPDATE CASCADE ON DELETE CASCADE, \
                seller_staff_id INTEGER DEFAULT NULL REFERENCES member(discord_id) \
                    ON UPDATE CASCADE ON DELETE CASCADE, \
                sold_date TEXT DEFAULT NULL, \
            PRIMARY KEY(number, category_id));")
c.execute("CREATE TABLE IF NOT EXISTS \
        sold (number TEXT NOT NULL, \
                identifier TEXT NOT NULL, \
                breed TEXT NOT NULL, \
                sex TEXT NOT NULL, \
                price INTEGER NOT NULL, \
                level INTEGER NOT NULL, \
                breedable INTEGER DEFAULT 0, \
                guild_id INTEGER REFERENCES whitelisted_guild(id), \
                reserver_id INTEGER REFERENCES member(discord_id), \
                reserve_staff_id INTEGER REFERENCES member(discord_id), \
                reserve_date TEXT NOT NULL, \
                buyer_id INTEGER REFERENCES member(discord_id), \
                seller_staff_id INTEGER REFERENCES member(discord_id), \
                sold_date TEXT NOT NULL);")
conn.commit()

# Load mysql database from schema
engine = create_engine('mysql+pymysql://' + DB_USER + ":" + DB_PASS + "@" + DB_HOST + '/' + DB_NAME, echo=True, pool_recycle=60, pool_pre_ping=True)
Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)
db = sessionmaker(bind=engine)

# Now start moving things over
# Start with guild table
guilds = c.execute("SELECT * FROM whitelisted_guild").fetchall()
with session_scope(db) as session:
    for guild in guilds:
        new_guild = Guild(
            discord_id=guild['id'],
            category_discord_id=guild['shop_category_id'],
            seller_role_discord_id=guild['accepted_role_id'],
            backend_channel_discord_id=guild['backend_channel_id'],
            whitelisted=True
        )
        session.add(new_guild)

# Now add members and update their many-to-many guild relationships
members = c.execute("SELECT * FROM member").fetchall()
guild_members = c.execute("SELECT * FROM guild_member").fetchall()
with session_scope(db) as session:
    for member in members:
        new_member = Member(discord_id=member['discord_id'], username=member['display_name'])
        guild = None
        for gm in guild_members:
            if gm['discord_id'] == member['discord_id']:
                guild = session.query(Guild).filter_by(discord_id=gm['guild_id']).one()
                new_member.guilds.append(guild)
        if guild is None:
            print(f"Guild not found for {member['display_name']}")
        session.add(new_member)

# Now transfer over item types (from category to types + channels)
categories = c.execute("SELECT * FROM category").fetchall()
with session_scope(db) as session:
    for cat in categories:
        guild = session.query(Guild).filter_by(discord_id=cat['guild_id']).one()
        channel = session.query(Channel).filter_by(discord_id=cat['channel_id']).first()
        if channel is None:
            channel = Channel(guild=guild, discord_id=cat['channel_id'])
        new_type = Type(
            guild_id=guild.id,
            sale_type=SaleType.ANIMAL,
            sub_name=cat['breed'],
            identifier=cat['identifier'],
            channel=channel
        )
        channel.types.append(new_type)
        session.add(new_type)

# Finally, transfer all sales/for-sale items over
animals = c.execute("SELECT * FROM animal").fetchall()
history = c.execute("SELECT * FROM sold").fetchall()
with session_scope(db) as session:
    for animal in animals:
        category = None
        for cat in categories:
            if cat['id'] == animal['category_id']:
                category = cat
                break
        if category is None:
            print(f"\n\nCould not find a category for at {animal.listing_id}\n\n")
            continue
        print("guild id:", category['guild_id'])
        print('sub name:', category['breed'])
        type_entry = session.query(Type).filter_by(sub_name=category['breed']).join(Guild).filter_by(discord_id=category['guild_id']).one()
        try:
            seller_entry = session.query(Member).filter_by(discord_id=animal['seller']).one()
        except Exception:
            # Create a new one,now that we know what guild they're in.
            # old_member = [member for member in members if member['discord_id'] == animal['seller']][0]
            guild = session.query(Guild).filter_by(discord_id=category['guild_id']).one()
            seller_entry = Member(discord_id=animal['seller'], username="--corruption--")
            seller_entry.guilds.append(guild)

        sku_id = category['identifier'] + animal['number']
        state = SaleState.NO_RESERVATIONS if animal['sale_state'] == State.FOR_SALE else SaleState.FULLY_RESERVED
        new_animal = Animal(
            sku_id=sku_id,
            type=type_entry,
            channel_id=type_entry.channel.id,
            seller=seller_entry,
            listing_discord_id=animal['listing_id'],
            sale_state=state,
            price_per=animal['price'],
            quantity_remaining=1,
            level=animal['level'],
            gender=animal['sex'][0].upper() + animal['sex'][1:].lower(),
            breedable=animal['breedable']
        )
        type_entry.current_number = max(type_entry.current_number, int(animal['number']))
        if animal['notes'] is not None:
            note = Note(sku=new_animal, content=animal['notes'], index=1)
            new_animal.notes.append(note)

        poster = session.query(Member).filter_by(discord_id=animal['seller'])
        one_day = timedelta(days=1)
        if animal['reserve_date'] is not None:
            reserve_time = datetime.strptime(animal['reserve_date'], '%m/%d/%Y')
            post_time = reserve_time - one_day
        else:
            post_time = datetime.now() - one_day

        posting = Posting(sku=new_animal, staff=seller_entry, time=post_time)
        new_animal.state_changes.append(posting)

        if animal['sale_state'] == State.RESERVED:
            reserve_staff = session.query(Member).filter_by(discord_id=animal['reserve_staff_id']).one()
            reserver = session.query(Member).filter_by(discord_id=animal['reserver_id']).one()
            reserve_time = datetime.strptime(animal['reserve_date'], '%m/%d/%Y')
            reservation = Reservation(
                sku=new_animal,
                staff=reserve_staff,
                time=reserve_time,
                reserver=reserver,
                reserve_number=1
            )
            new_animal.state_changes.append(reservation)
            new_animal.quantity_remaining = 0
            new_animal.sale_state = SaleState.FULLY_RESERVED

        session.add(new_animal)
