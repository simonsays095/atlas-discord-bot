import discord
from discord.ext import commands

'''
Adds several converter classes, to search for discord objects case-insensitively.
'''


class CIRole(commands.RoleConverter):
    async def convert(self, ctx, argument):
        role = None
        try:
            role = await super().convert(ctx, argument)
            return role
        except commands.CommandError:
            # Now try it case insensitively
            roles = ctx.guild.roles
            for r in roles:
                print(r.name.lower())
                if r.name.lower() == argument.lower():
                    return r
        return argument


class CIChannel(commands.TextChannelConverter):
    async def convert(self, ctx, argument):
        channel = None
        try:
            channel = await super().convert(ctx, argument)
            return channel
        except commands.CommandError:
            # Now try it case insensitively
            channels = ctx.guild.channels
            for c in channels:
                if c.name.lower() == argument.lower():
                    return c
        return argument


class DefaultMember():
    id = 0
    display_name = "Unknown"
    discriminator = "0000"


DEFAULT_MEMBER_ALIASES = ['anon', 'unknown', 'anonymous']


class CIMember(commands.MemberConverter):
    async def convert(self, ctx, argument):
        member = None
        try:
            member = await super().convert(ctx, argument)
        except commands.CommandError:
            # Try it case insensitively now
            members = ctx.guild.members
            for m in members:
                if m.display_name.lower() == argument.lower or m.name.lower() == argument.lower():
                    member = m
                    break

        # now check for default user aliases
        if member is None:
            if argument.lower() in DEFAULT_MEMBER_ALIASES:
                member = DefaultMember

        if member is None:
            raise commands.CommandError("Member " + argument + " not found.")

        return member


class CICategory(commands.CategoryChannelConverter):
    async def convert(self, ctx, argument):
        category = None
        try:
            category = await super().convert(ctx, argument)
        except commands.CommandError:
            # Try it case insensitively now
            channels = ctx.guild.channels
            for c in channels:
                print(c, c.id)
                if not isinstance(c, discord.CategoryChannel):
                    continue
                print(c.name.lower())
                print(argument.lower())
                if c.name.lower() == argument.lower():
                    return c
            # raise commands.CommandError("Category " + argument + " not found.")
            return argument

        return category
