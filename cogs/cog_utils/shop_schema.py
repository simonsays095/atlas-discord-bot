import os
import enum
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy import ForeignKey
from sqlalchemy import Table, Column, Integer, String, Boolean
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.types import BigInteger, DateTime, Enum, Interval
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_continuum import make_versioned
from dotenv import load_dotenv

make_versioned(user_cls=None)
load_dotenv()

DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')
DB_HOST = os.getenv('DB_HOST')

engine = create_engine('mysql+pymysql://' + DB_USER + ":" + DB_PASS + "@" + DB_HOST + '/' + DB_NAME, pool_pre_ping=True)
Base = declarative_base()


# ------------------------------------- #
# DB representations of discord classes #
# ------------------------------------- #


Guild_Member = Table('guild_member', Base.metadata,
                     Column('guild_id', Integer, ForeignKey('guild.id')),
                     Column('member_id', Integer, ForeignKey('member.id'))
                     )


class Guild(Base):
    __tablename__ = 'guild'

    id = Column(Integer, primary_key=True)
    discord_id = Column(BigInteger)
    category_discord_id = Column(BigInteger)
    seller_role_discord_id = Column(BigInteger)
    backend_channel_discord_id = Column(BigInteger)
    whitelisted = Column(Boolean)
    autoincrement = Column(Boolean, default=True)
    public = Column(Boolean, default=False)

    members = relationship("Member", secondary=Guild_Member)
    channels = relationship("Channel", back_populates="guild")
    types = relationship("Type", back_populates="guild")
    auctions = relationship("Auction", back_populates="guild")


class Member(Base):
    __tablename__ = 'member'

    id = Column(Integer, primary_key=True)
    discord_id = Column(BigInteger)
    username = Column(String(256))

    facilitated_transactions = relationship("StateChange", back_populates="staff", foreign_keys="StateChange.staff_id")
    reservations = relationship("Reservation", back_populates="reserver", foreign_keys="Reservation.reserver_id")
    unreservations = relationship("UnReservation", back_populates="unreserver", foreign_keys="UnReservation.unreserver_id")
    purchases = relationship("Finalization", back_populates="buyer", foreign_keys="Finalization.buyer_id")
    guilds = relationship(Guild, secondary=Guild_Member)
    skus = relationship("Sku", back_populates="seller")

    def get_name(self, bot, use_mention=True):
        reserver = bot.get_user(self.discord_id)
        # return f"{reserver.display_name}#{reserver.discriminator}" if reserver is not None else self.username
        name = self.username
        if reserver is not None:
            name = reserver.mention if use_mention else f"{reserver.display_name}#{reserver.discriminator}"
        return name

    def get_total_reservations(self, sku_id):
        total = 0
        print(f"reservations for {self.username}:")
        for r in self.reservations:
            print(f"{r.sku.sku_id}: {r.reserve_number}")
            if r.sku.id == sku_id:
                total += r.reserve_number
        print(f"unreservations for {self.username}:")
        for u in self.unreservations:
            print(f"{u.sku.sku_id}: {u.unreserve_number}")
            if u.sku.id == sku_id:
                total -= u.unreserve_number
        print(f"purchases for {self.username}:")
        for p in self.purchases:
            print(f"{p.sku.sku_id}: {p.purchase_amount}")
            if p.sku.id == sku_id:
                total -= p.purchase_amount
        return total


class Channel(Base):
    __tablename__ = 'channel'

    id = Column(Integer, primary_key=True)
    guild_id = Column(Integer, ForeignKey(Guild.id))
    discord_id = Column(BigInteger)
    deleted = Column(Boolean, default=False)

    guild = relationship(Guild, back_populates='channels')
    skus = relationship('Sku')
    types = relationship('Type', back_populates="channel")


# ----------------------------------------------- #
#         Representations of sale items           #
# ----------------------------------------------- #


class SaleType(enum.Enum):
    ITEM = "item"
    ANIMAL = "animal"


class SaleState(enum.Enum):
    CANCELLED = "cancelled"
    NO_RESERVATIONS = "no_reservations"
    ONE_RESERVATION = "one_reservation"
    MULTIPLE_RESERVATIONS = "multiple_reservations"
    FULLY_RESERVED = "fully_reserved"
    SOLD_OUT = "sold_out"


class Type(Base):
    __tablename__ = 'type'

    id = Column(Integer, primary_key=True)
    guild_id = Column(Integer, ForeignKey(Guild.id))
    sale_type = Column(Enum(SaleType))
    sub_name = Column(String(128))
    identifier = Column(String(8))
    current_number = Column(Integer, default=1)
    channel_id = Column(Integer, ForeignKey(Channel.id))

    channel = relationship(Channel, back_populates="types")
    guild = relationship(Guild, back_populates="types")
    skus = relationship('Sku', back_populates="type")
    __table_args__ = (UniqueConstraint('sub_name', 'guild_id', name='_typename_guild_uc'),)


class Sku(Base):
    __versioned__ = {}
    __tablename__ = "sku"

    id = Column(Integer, primary_key=True)
    sku_id = Column(String(32))
    type_id = Column(Integer, ForeignKey(Type.id))
    channel_id = Column(Integer, ForeignKey(Channel.id))
    seller_id = Column(Integer, ForeignKey(Member.id))
    price_per = Column(Integer)
    quantity_remaining = Column(Integer)
    sale_type = Column(Enum(SaleType))
    listing_discord_id = Column(BigInteger)
    sale_state = Column(Enum(SaleState), default=SaleState.NO_RESERVATIONS)
    IsInAuction = Column(Boolean, default=False)

    state_changes = relationship("StateChange", back_populates="sku")
    notes = relationship("Note", back_populates="sku")
    type = relationship(Type, back_populates="skus")
    seller = relationship(Member, back_populates="skus")

    __table_args__ = (UniqueConstraint('sku_id', 'type_id', name='_sku_type_uc'),)

    __mapper_args__ = {
        'polymorphic_identity': 'sku',
        'polymorphic_on': 'sale_type'
    }

    def ReservingMembers(self):
        member_list = []
        if self.sale_state == SaleState.CANCELLED:
            return member_list
        for t in self.state_changes:
            if t.transaction_type != Transactions.RESERVATION:
                continue
            if t.reserver not in member_list and t.reserver.get_total_reservations(self.id) > 0:
                member_list.append(t.reserver)
        return member_list

    def CreateCollapsedReservationList(self, bot, **kwargs):
        reserve_list = []
        for member in self.ReservingMembers():
            reserve_list.append(f"{member.get_name(bot, **kwargs)}: {member.get_total_reservations(self.id)}")
        return reserve_list

    def SetState(self):
        reservers = self.ReservingMembers()
        if len(reservers) == 0:
            if self.quantity_remaining == 0:
                self.sale_state = SaleState.SOLD_OUT
            else:
                self.sale_state = SaleState.NO_RESERVATIONS
        elif self.quantity_remaining == 0:
            self.sale_state = SaleState.FULLY_RESERVED
        elif len(reservers) == 1:
            self.sale_state = SaleState.ONE_RESERVATION
        else:
            self.sale_state = SaleState.MULTIPLE_RESERVATIONS


class Rarity(enum.Enum):
    NONE = ("None", ["none"], 4289797)
    COMMON = ("Common", ["common", "c"], 8421504)
    FINE = ("Fine", ["fine", "f"], 65280)
    JOURNEYMAN = ("Journeyman", ["journeyman", "j", "jm"], 255)
    MASTERWORK = ("Masterwork", ["masterwork", "ma"], 16711935)
    LEGENDARY = ("Legendary", ["legendary", "l"], 16776960)
    MYTHICAL = ("Mythical", ["mythical", "my"], 4315866)

    def __init__(self, print_name, strings, embed_color):
        self.print_name = print_name
        self.strings = strings
        self.embed_color = embed_color

    @staticmethod
    def from_string(string):
        for val in Rarity:
            if string.lower() in val.strings:
                return val

        raise KeyError("Invalid rarity.")


class Item(Sku):
    rarity = Column(Enum(Rarity), default=Rarity.COMMON)
    blueprint = Column(Boolean)

    __mapper_args__ = {
        'polymorphic_identity': SaleType.ITEM
    }


class Animal(Sku):
    level = Column(Integer)
    gender = Column(String(16))
    breedable = Column(Boolean)

    __mapper_args__ = {
        'polymorphic_identity': SaleType.ANIMAL
    }


class Note(Base):
    __tablename__ = 'note'

    id = Column(Integer, primary_key=True)
    sku_id = Column(Integer, ForeignKey(Sku.id))
    content = Column(String(256))
    index = Column(Integer)

    sku = relationship(Sku, back_populates='notes')

    __table_args__ = (UniqueConstraint('sku_id', 'index', name='_sku_noteindex_uc'),)


# -------------------------------------- #
#     User changes to sale item state    #
# -------------------------------------- #


class Transactions(enum.Enum):
    CANCELLATION = "Cancellation"
    POSTING = "Posting"
    # EDIT = "Edit"
    RESERVATION = "Reservation"
    UNRESERVATION = "Uneservation"
    FINALIZE = "Finalize"


class StateChange(Base):
    __tablename__ = "state_change"

    id = Column(Integer, primary_key=True)
    transaction_type = Column(Enum(Transactions))
    sku_id = Column(Integer, ForeignKey(Sku.id))
    staff_id = Column(Integer, ForeignKey(Member.id))
    # time = Column(DateTime, nullable=False)
    time = Column(DateTime)

    sku = relationship(Sku, back_populates="state_changes")
    staff = relationship(Member, back_populates="facilitated_transactions", foreign_keys=staff_id)

    __mapper_args__ = {
        'polymorphic_identity': 'state_change',
        'polymorphic_on': 'transaction_type'
    }


class Cancellation(StateChange):
    # Just for queries and organization - no special attributes yet

    __mapper_args__ = {
        'polymorphic_identity': Transactions.CANCELLATION
    }


class Posting(StateChange):

    __mapper_args__ = {
        'polymorphic_identity': Transactions.POSTING
    }


class Reservation(StateChange):
    reserver_id = Column(Integer, ForeignKey(Member.id))
    reserve_number = Column(Integer)

    reserver = relationship(Member, back_populates="reservations", foreign_keys=reserver_id)

    __mapper_args__ = {
        'polymorphic_identity': Transactions.RESERVATION
    }


class UnReservation(StateChange):
    unreserver_id = Column(Integer, ForeignKey(Member.id))
    unreserve_number = Column(Integer)

    unreserver = relationship(Member, back_populates="unreservations", foreign_keys=unreserver_id)

    __mapper_args__ = {
        'polymorphic_identity': Transactions.UNRESERVATION
    }


class Finalization(StateChange):
    buyer_id = Column(Integer, ForeignKey(Member.id))
    purchase_amount = Column(Integer)

    buyer = relationship(Member, back_populates="purchases", foreign_keys=buyer_id)

    __mapper_args__ = {
        'polymorphic_identity': Transactions.FINALIZE
    }


class Auction(Base):
    __tablename__ = 'auction'

    id = Column(Integer, primary_key=True)
    guild_id = Column(Integer, ForeignKey(Guild.id))
    channel_discord_id = Column(BigInteger)
    sku_id = Column(String(64), ForeignKey(Sku.sku_id))
    timeStart = Column(DateTime, default=datetime.now())
    duration = Column(Interval)
    hidden = Column(Boolean)
    active = Column(Boolean)

    bids = relationship('Bid', back_populates="auction")
    guild = relationship(Guild, back_populates="auctions")


class Bid(Base):
    __tablename__ = 'bid'

    id = Column(Integer, primary_key=True)
    auction_id = Column(Integer, ForeignKey(Auction.id))
    amount = Column(Integer)
    bidder_id = Column(Integer, ForeignKey(Member.id))
    time = Column(DateTime, default=datetime.now())

    auction = relationship(Auction, back_populates='bids')
    bidder = relationship(Member)


Base.metadata.create_all(engine)
