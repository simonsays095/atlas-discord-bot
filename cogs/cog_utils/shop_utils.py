import enum
import discord
from .shop_schema import SaleState, SaleType, Rarity
from ATLAS_utils import Message


class State(enum.Enum):
    FAILURE = -1
    WARNING = 0
    SUCCESS = 1


class ConvertArg:
    def __init__(self, converter_class):
        self.converter_class = converter_class


class SkuEmbed:
    '''
    A class to help the editing and updating of embeds.

    Embeds for the shop are broken up into the following categories, which can be set independently:
    - ID
    - Price
    - Quantity
    - Notes
    - Reservations

    Quantity and notes are in a single field, and reservations are in another.
    The functions ForSale and AllReserved will change various attributes to show when no sale items are available.
    '''
    def __init__(self, bot, sku, image_url=None):
        self.embed = discord.Embed()
        self.bot = bot
        self.sku = sku
        if image_url is not None:
            self.embed.set_image(url=image_url)

        # Private variables
        self._more_info_idx = None
        self._more_info_exists = True  # Assume it does

    @classmethod
    def from_message(cls, message, bot, sku):
        image = message.attachments[0].url if len(message.attachments) > 0 else None
        skuEmbed = cls(bot, sku, image)
        if len(message.embeds) > 0:
            skuEmbed.embed = message.embeds[0]
        else:
            return SkuEmbed.create_new(bot, sku, image)
        return skuEmbed

    @classmethod
    def create_new(cls, bot, sku, image=None):
        print("creating a new embed")
        skuEmbed = cls(bot, sku, image)
        skuEmbed.ForSale()
        skuEmbed.SetPrice()
        skuEmbed.SetQuantity()
        skuEmbed.SetNotes()
        print("finished creating the embed")
        print(skuEmbed.embed.to_dict())
        return skuEmbed

    def SetID(self):
        '''
        Includes the author's profile pic, sku ID, and title (short description).
        '''
        print("setting ID")
        author = self.bot.get_user(self.sku.seller.discord_id)
        ID = self.sku.sku_id
        if self.sku.sale_state == SaleState.FULLY_RESERVED:
            ID += " -- RESERVED"
        self.embed.set_author(name=ID, icon_url=author.avatar_url)
        print(self.sku.type.sale_type)
        if self.sku.type.sale_type == SaleType.ANIMAL:
            print("This is an animal")
            title = "Level " + str(self.sku.level)
            if self.sku.gender != "none":
                title += " " + self.sku.gender
            title += " " + self.sku.type.sub_name
        else:
            title = self.sku.type.sub_name
            if self.sku.blueprint:
                title += " Blueprint"
        self.embed.title = title
        print("finished with ID")
        print(self.embed.to_dict())

    def SetPrice(self):
        '''
        In the future, will include a striked-out original price if it has changed.
        '''
        print("setting price")
        prices = []
        for ver in self.sku.versions:
            if ver.price_per == -1:
                prices = [self.sku.versions[0].price_per]
            else:
                prices.append(ver.price_per)
        price_str = ""
        for p in prices[:-1]:
            price_str += "~~" + str(p) + "g~~ "
        price_str += str(prices[-1]) + "g"
        self.embed.description = price_str
        print("finished with price")
        print(self.embed.to_dict())

    def SetReservations(self):
        print("setting reservations")
        reserve_list = self.sku.CreateCollapsedReservationList(self.bot)
        print("reservations list:")
        print(reserve_list)
        found = False
        for idx, f in enumerate(self.embed.fields):
            if f.name == "Reservations:":
                found = True
                if reserve_list:
                    self.embed.set_field_at(idx, name=f.name, value="\n".join(reserve_list), inline=f.inline)
                else:
                    self.embed.remove_field(idx)

        if not found and reserve_list:
            self.embed.add_field(name="Reservations:", value="\n".join(reserve_list), inline=False)
        print("finished with reservations")
        print(self.embed.to_dict())

    def SetQuantity(self):
        print("setting quantity")
        quantity = f"Quantity left: {self.sku.quantity_remaining}"
        if self.more_info_idx is not None:
            more_info = self.embed.fields[self.more_info_idx]
            lines = more_info.value.split("\n")
            found_quantity_line = False
            for idx, line in enumerate(lines):
                if line.startswith("Quantity left: "):
                    found_quantity_line = True
                    lines[idx] = quantity
                    break
            if not found_quantity_line:
                lines.append(quantity)
            self.embed.set_field_at(self.more_info_idx, name=more_info.name, value="\n".join(lines), inline=more_info.inline)
        else:
            self.embed.add_field(name="More info:", value=quantity)
            self.more_info_idx = len(self.embed.fields) - 1

    def SetNotes(self):
        '''
        Includes actual notes in their own field, as well as the breedable status for animals and rarity for items.
        '''
        print("setting notes")
        if self.sku.type.sale_type == SaleType.ANIMAL:
            # Add breedable status to the more info field
            if self.sku.breedable:
                breedable = "Breedable: :white_check_mark:"
            else:
                breedable = "Breedable: :x:"

            if self.more_info_idx is not None:
                more_info = self.embed.fields[self.more_info_idx]
                lines = more_info.value.split("\n")
                found_breedable_line = False
                for idx, line in enumerate(lines):
                    if line.startswith("Breedable:"):
                        lines[idx] = breedable
                        found_breedable_line = True
                        break
                if not found_breedable_line:
                    lines.append(breedable)
                self.embed.set_field_at(self.more_info_idx, name=more_info.name, value="\n".join(lines), inline=more_info.inline)
            else:
                self.embed.add_field(name="More info:", value=breedable)
                self.more_info_idx = len(self.embed.fields) - 1
        else:
            # Add rarity status to the more info field, if included
            if self.sku.rarity is not None and self.sku.rarity != Rarity.NONE:
                rarity_line = "Rarity: " + self.sku.rarity.print_name
                if self.more_info_idx is not None:
                    more_info = self.embed.fields[self.more_info_idx]
                    lines = more_info.value.split("\n")
                    found_rarity_line = False
                    for idx, line in enumerate(lines):
                        if line.startswith("Rarity: "):
                            line = rarity_line
                            found_rarity_line = True
                            break
                    if not found_rarity_line:
                        lines.append(rarity_line)
                    self.embed.set_field_at(self.more_info_idx, name=more_info.name, value="\n".join(lines), inline=more_info.inline)
                else:
                    self.embed.add_field(name="More info:", value=rarity_line)
                    self.more_info_idx = len(self.embed.fields) - 1
        print("making contents")
        note_contents = [f"{note.index}) {note.content}" for note in self.sku.notes]
        print("notes:", note_contents)
        print("got em")

        found_notes = False
        for idx, field in enumerate(self.embed.fields):
            if field.name == "Notes:":
                if note_contents:
                    print("setting notes field")
                    self.embed.set_field_at(idx, name="Notes:", value="\n".join(note_contents))
                else:
                    print("removing notes field")
                    self.embed.remove_field(idx)
                found_notes = True
                break
        if not found_notes and note_contents:
            print("Creating notes field")
            self.embed.add_field(name="Notes:", value="\n".join(note_contents))
        print("finished with notes")

    @property
    def more_info_idx(self):
        print("getting more info index")
        # If we couldn't find it before, don't look again
        if self._more_info_exists is False:
            print("We already determined it doesn't exist")
            return None

        # Get it from memory
        if self._more_info_idx is not None:
            print(f"already had it as {self._more_info_idx}")
            return self._more_info_idx

        # Find it in the embed
        print("Trying to find it in the embed")
        for idx, f in enumerate(self.embed.fields):
            if f.name == "More info:":
                self._more_info_idx = idx
                break

        if self._more_info_idx is None:
            print("We looked, and we couldn't find it")
            self._more_info_exists = False

        print(f"returning this index: {self._more_info_idx}")
        return self._more_info_idx

    @more_info_idx.setter
    def more_info_idx(self, value):
        self._more_info_idx = value
        self._more_info_exists = True

    def AllReserved(self):
        self.embed.color = 13632027  # Darkish red - reserved
        self.SetID()
        self.embed.set_footer(text=f"No remaining {self.sku.sale_type.value}s for sale.")

    def ForSale(self):
        self.SetID()
        if self.sku.type.sale_type == SaleType.ANIMAL:
            self.embed.color = 4289797  # Dark green - for sale
        else:
            self.embed.color = self.sku.rarity.embed_color

        seller = self.sku.seller
        self.embed.set_footer(text=f"For more info, ask {seller.get_name(self.bot, use_mention=False)}.")


class ArgParser:
    def __init__(self, ctx):
        self.ctx = ctx
        self.requiredPatterns = {}
        self.optionalPatterns = {}

    async def Parse(self, args):
        '''
        Parses the arguments supplied according to the requirePatterns and optionPatterns dictionaries.

        If not all of the arguments supplied match (either required or optional) arguments, will raise an error.
        If not all requiredPatterns are found, it will also raise an error.
        '''
        print("parsing")
        parsedArgs = {}
        argFound = [False] * len(args)
        message = Message(self.ctx)
        message.raw = True
        for key, pattern in self.requiredPatterns.items():
            print(f"looking for {key}")
            found = False
            for idx, arg in enumerate(args):
                if isinstance(pattern, ConvertArg):
                    print("converting argument")
                    try:
                        converted = await pattern.converter_class().convert(self.ctx, arg)
                        if converted != arg:
                            found = True
                            parsedArgs[key] = converted
                            argFound[idx] = True
                            break
                    except Exception:  # Let it go
                        pass
                elif pattern.match(arg):
                    found = True
                    parsedArgs[key] = arg
                    argFound[idx] = True
                    break
            assert found, f"Required argument \"{key}\" not found."

        print("looking for optional patterns")
        for key, pattern in self.optionalPatterns.items():
            found = False
            for idx, arg in enumerate(args):
                if isinstance(pattern, ConvertArg):
                    print("converting argument")
                    try:
                        converted = await pattern.converter_class().convert(self.ctx, arg)
                        if converted != arg:
                            found = True
                            parsedArgs[key] = converted
                            argFound[idx] = True
                            break
                    except Exception:  # Let it go
                        pass
                elif pattern.match(arg):
                    parsedArgs[key] = arg
                    found = True
                    argFound[idx] = True
                    break
            if not found:
                parsedArgs[key] = None
        assert all(argFound), f"Argument{'s' if sum(argFound) > 1 else ''} invalid: {','.join([arg for idx, arg in enumerate(args) if argFound[idx] is False])}"

        return parsedArgs
