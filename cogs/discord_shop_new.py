import os
import inspect
import boto3
import discord
import re
from contextlib import contextmanager
from datetime import datetime, timedelta
from discord import PermissionOverwrite
from discord.ext import commands
from ATLAS_utils import PrintTo, GetMatPrintString, Message
from sqlalchemy.orm import sessionmaker, subqueryload
from sqlalchemy.orm.exc import NoResultFound  # , MultipleResultsFound
from sqlalchemy import create_engine

# Custom imports from other files
# sqlalchemy schemas and extra types
from .cog_utils.shop_schema import Guild, Channel, Member, Sku, Animal, Item, Type, Rarity, Note
from .cog_utils.shop_schema import SaleType, SaleState, Transactions  # Enums
from .cog_utils.shop_schema import Cancellation, Posting, Reservation, UnReservation, Finalization

# Helper classes and functions
from .cog_utils.converters import CIRole, CICategory, CIChannel, CIMember, DefaultMember
from .cog_utils.shop_utils import State, SkuEmbed, ArgParser, ConvertArg


BREEDABLE_OPTIONS = ["true", "breedable"]
NON_BREEDABLE_OPTIONS = ["false", "neutered", "spayed"]


@contextmanager
def session_scope(db, query=False):
    """Provide a transactional scope around a series of operations."""
    session = db()
    try:
        yield session
        if not query:
            session.commit()
    except Exception as e:
        session.rollback()
        print(e)
        raise e
    finally:
        session.close()


def IsWhitelisted(ctx):
    # Get the guild entry from db
    with session_scope(ctx.cog.db, query=True) as session:
        guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()

        if guild_entry is None or not guild_entry.whitelisted:
            raise commands.CheckFailure("This guild is not whitelisted.")

    return True


def CanSell(ctx):
    '''
    Check to see if this person can sell things. For public servers, anyone can. For private servers, admins and sellers can.
    '''
    guild = ctx.guild
    author = ctx.message.author
    # Admins can always sell
    if author.guild_permissions.administrator:
        print(f"{ctx.author.display_name} is an admin, so they can sell.")
        return True

    # Query db for guild info first
    with session_scope(ctx.cog.db) as session:
        guild_entry = session.query(Guild).filter_by(discord_id=guild.id).one()
        if guild_entry.public:
            print(f"{ctx.guild.name} is a public server - {ctx.author.display_name} can sell.")
            return True

        # If this person has the seller role, they can sell
        seller_role = discord.utils.get(guild.roles, id=guild_entry.seller_role_discord_id)
        if seller_role in author.roles:
            print(f"{ctx.author.display_name} has the seller role, so they can sell.")
            return True

    print(f"{ctx.author.display_name} cannot sell on this server.")
    raise commands.CheckFailure(f"Only members with the \"{seller_role.name}\" role can make sales on this server.")


def CanAlterSale(ctx):
    '''
    Check to see if this person is allowed to alter the db entry for an item (in any of several ways).

    The seller, admins, and people with the seller role can alter sales.
    '''
    if ctx.invoked_subcommand is None:
        return True  # Let each command handle it individually.
    guild = ctx.guild
    author = ctx.message.author
    # Admins can always alter sales
    if author.guild_permissions.administrator:
        print(f"{ctx.author.display_name} is an admin, so they alter sales.")
        return True

    command = ctx.prefix + ctx.invoked_subcommand.qualified_name + " "
    args = ctx.message.content.replace(command, "")
    sku = args.split(" ")[0]

    with session_scope(ctx.cog.db) as session:
        guild_entry = session.query(Guild).filter_by(discord_id=guild.id).one()

        # If they have the seller role, they can alter sales
        seller_role = discord.utils.get(guild.roles, id=guild_entry.seller_role_discord_id)
        if seller_role in author.roles:
            print(f"{ctx.author.display_name} has the seller role, so they can alter sales.")
            return True

        # If they are the seller, they can alter this sale
        sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(id=guild_entry.id).one_or_none()
        if sku_entry is None:
            return True  # This sku doesn't exist, but let the command take care of the message to avoid double error messages.
        if sku_entry.seller.discord_id == author.id:
            print(f"{ctx.author.display_name} is the seller for this sale item, so they can alter it.")
            return True

    raise commands.CheckFailure(f"Only members with the \"{seller_role.name}\" role can alter sales on this server.")


def IsInBackend(ctx):
    '''
    Predicate for a command check: See if you are in the backend channel for this server.
    '''
    with session_scope(ctx.cog.db) as session:
        guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).one()
        if guild_entry.backend_channel_discord_id is None:
            raise commands.CheckFailure("A backend channel needs to be set up before using this command. Set it up with `-shop setup backend ...`.")
        if guild_entry.backend_channel_discord_id != ctx.channel.id:
            backend_channel = ctx.cog.bot.get_channel(guild_entry.backend_channel_discord_id)
            raise commands.CheckFailure(f"This command can only be used in the backend channel for this guild, {backend_channel.mention}.")

    return True


def emojiResponse(func):
    '''
    Decorator for commands, which will react to the original post with the outcome of the command.

    Currently set up for success, warning, and failure, from the State Enum above.
    '''
    async def emojiDecorator(cog_obj, ctx, *args, **kwargs):
        result = await func(cog_obj, ctx, *args, **kwargs)
        if result == State.FAILURE:
            await ctx.message.add_reaction(u"\u274C")  # Cross mark
        elif result == State.WARNING:
            await ctx.message.add_reaction(u"\u26A0")  # warning sign
        elif result == State.SUCCESS:
            await ctx.message.add_reaction(u"\u2705")  # heavy white check mark
        else:
            pass  # Don't know what to react with, so don't react
    emojiDecorator.__name__ = func.__name__
    emojiDecorator.__signature__ = inspect.signature(func)
    emojiDecorator.__doc__ = func.__doc__
    return emojiDecorator


def GetBotCost():
    client = boto3.client('ce')
    # Add 2 days to get around time-zone issues. Precise results not needed anyway.
    startdate = (datetime.now() + timedelta(days=2)).strftime("%Y-%m-%d")
    enddate = (datetime.now() + timedelta(weeks=4, days=2)).strftime("%Y-%m-%d")
    costs = client.get_cost_forecast(
        TimePeriod={
            'Start': startdate,
            'End': enddate
        },
        Metric="BLENDED_COST",
        Granularity="MONTHLY"
    )
    return float(costs['Total']['Amount'])


async def SetBackendPermissions(ctx, backend_channel, guild_entry):
    botRole = next((role for role in ctx.guild.me.roles if role.managed), None)
    assert botRole is not None, "The bot must have a managed role - you have to re-add the bot to this server to get this role."

    noread = PermissionOverwrite(read_messages=False)
    yesread = PermissionOverwrite(read_messages=True)

    overwrites = {
        ctx.guild.default_role: yesread if guild_entry.public else noread,
        botRole: yesread
    }

    if guild_entry.seller_role_discord_id is not None:
        seller_role = await CIRole().convert(ctx, str(guild_entry.seller_role_discord_id))
        overwrites.update({seller_role: yesread})

    await backend_channel.edit(overwrites=overwrites)


class DiscordShopCog(commands.Cog, name='Discord Shop'):
    '''
    Main cog that handles all shop commands.

    Most commands have the following structure:

    1) Accept and parse input from discord
    2) Query the database if necessary
    3) Update discord - change channel structure and/or print warnings, successes, or failures.
    4) Update database if necessary
    '''

    def __init__(self, bot, **kwargs):
        self.bot = bot
        self.donateLastUpdated = datetime.now() - timedelta(weeks=4)  # Always update when the bot is first loaded.
        self.botMonthlyPrice = 0
        DB_NAME = os.getenv('DB_NAME')
        DB_USER = os.getenv('DB_USER')
        DB_PASS = os.getenv('DB_PASS')
        DB_HOST = os.getenv('DB_HOST')

        # Set up the database
        self.engine = create_engine('mysql+pymysql://' + DB_USER + ":" + DB_PASS + "@" + DB_HOST + '/' + DB_NAME, pool_recycle=60, pool_pre_ping=True)
        print("creating base")
        from .cog_utils.shop_schema import Base
        Base.metadata.create_all(self.engine)
        print("created all")
        self.db = sessionmaker(bind=self.engine)

    def check_db(self):
        '''
        When starting up, make sure all references to discord IDs are valid.
        '''
        # Check to make sure we're still in each guild
        print("checking db")
        with session_scope(self.db) as session:
            print("guilds")
            guilds_in_db = session.query(Guild).all()
            discord_guild_ids = [guild.id for guild in self.bot.guilds]

            # Check if we left any guilds
            print("more guilds")
            for db_guild in guilds_in_db:
                if db_guild.id not in discord_guild_ids:
                    self.remove_guild_by_id(db_guild.id, session)

            # Check if we joined any guilds
            print("even more guilds")
            db_guild_ids = [guild.id for guild in guilds_in_db]
            for bot_guild in discord_guild_ids:
                if bot_guild not in db_guild_ids:
                    self.add_guild_by_id(bot_guild, session)

        # Check to make sure all channels are relevant
        with session_scope(self.db) as session:
            # Start with sales channels
            print("channels")
            channels_in_db = session.query(Channel).all()
            for channel in channels_in_db:
                discord_channel = self.bot.get_channel(id=channel.discord_id)
                if discord_channel is None:
                    channel.deleted = True

            # Now check the backend channel
            print("backend")
            guilds_in_db = session.query(Guild).all()
            for guild in guilds_in_db:
                if guild.backend_channel_discord_id is not None:
                    backend = self.bot.get_channel(id=guild.backend_channel_discord_id)
                    if backend is None:
                        guild.backend_channel_discord_id = None

    def remove_guild_by_id(self, guild_id, session):
        db_guild = session.query(Guild).filter_by(discord_id=guild_id).one_or_none()
        if db_guild is not None:
            db_guild.whitelisted = False

    def add_guild_by_id(self, guild_id, session):
        db_guild = session.query(Guild).filter_by(discord_id=guild_id).one_or_none()
        if db_guild is None:
            new_guild = Guild(discord_id=guild_id, whitelisted=False)
            session.add(new_guild)

    def Add_member_to_DB(self, guild, member, only_update=False):
        '''
        Adds a given Member object to the database. only_update should be selected if you only want to update and entry,
        and ignore it if the entry does not already exist (mainly used for on_member_update).
        '''
        assert isinstance(guild, discord.Guild), "Guild must be a discord.Guild object."
        assert isinstance(member, discord.Member) or member is DefaultMember, "member must be a discord.Member object."
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=guild.id).one()
            new_member = Member(discord_id=member.id, username=member.display_name)
            exists = False
            for db_member in guild_entry.members:
                if db_member.discord_id == member.id:
                    db_member.username = member.display_name
                    exists = True
                    break
            if not exists and not only_update:
                session.add(new_member)
                guild_entry.members.append(new_member)

# ----------------------------------------------------------- #
#                 Temporary testing commands                  #
# ----------------------------------------------------------- #

    @commands.command()
    @commands.is_owner()
    @emojiResponse
    async def drop(self, ctx):
        '''
        DROPS ALL DATABASE DATA -- DO NOT USE LIGHTLY
        '''
        from .cog_utils.shop_schema import Base
        Base.metadata.drop_all(self.engine)
        try:
            self.bot.reload_extension('cogs.discord_shop_new')
            self.check_db()
        except Exception as e:
            await PrintTo(ctx, '{}: {}'.format(type(e).__name__, e))
            return State.FAILURE
        return State.SUCCESS

    @commands.command()
    @commands.is_owner()
    @emojiResponse
    async def channels(self, ctx):
        message = Message(ctx)
        # 1) Accept and parse input from discord -- No action required
        # 2) Query the database
        with session_scope(self.db, query=True) as session:
            guilds = session.query(Guild).options(
                subqueryload(Guild.channels).subqueryload(Channel.types)
            ).all()

        # 3) Update discord
        if len(guilds) == 0:
            message.message = "No channels are listed in the database."
            message.raw = True
            await message.Print()
            return State.WARNING

        mat = [['Guild Name', 'Channel Name', 'Deleted', 'Types']]
        for guild in guilds:
            if guild.discord_id not in [discord_guild.id for discord_guild in self.bot.guilds]:
                guild_name = guild.discord_id
                backend_channel_name = guild.backend_channel_discord_id
                deleted = None
                types = None
            else:
                g = await self.bot.fetch_guild(guild.discord_id)
                guild_name = g.name
                if guild.backend_channel_discord_id is None:
                    backend_channel_name = None
                else:
                    backend_channel = self.bot.get_channel(id=guild.backend_channel_discord_id)
                    backend_channel_name = backend_channel.name

                mat.append([guild_name, backend_channel_name, "N/A", "N/A"])

                # All other (sales) channels
                for channel in guild.channels:
                    if channel.deleted:
                        continue
                    channel_name = self.bot.get_channel(id=channel.discord_id).name
                    deleted = channel.deleted
                    types = ", ".join(db_type.sub_name for db_type in channel.types)

                    mat.append([guild_name, channel_name, deleted, types])

        message.message = GetMatPrintString(mat)
        await message.Print()
        # 4) Update database -- No action required
        return State.SUCCESS

    @commands.command()
    @commands.is_owner()
    @emojiResponse
    async def types(self, ctx):
        message = Message(ctx)
        # 1) Accept and parse input from discord -- No action required
        # 2) Query the database
        with session_scope(self.db, query=True) as session:
            types = session.query(Type).options(
                subqueryload(Type.channel)
                .subqueryload(Channel.guild)
            ).all()

        # 3) Update discord
        if len(types) == 0:
            message.message = "No types are listed in the database."
            message.raw = True
            await message.Print()
            return State.WARNING

        mat = [['Guild Name', 'Channel Name', 'Type', "Sub-name", 'Identifier']]
        for t in types:
            db_guild = t.channel.guild
            if db_guild.discord_id not in [discord_guild.id for discord_guild in self.bot.guilds]:
                guild_name = db_guild.discord_id
                mat.append([guild_name, t.channel.discord_id, t.sale_type.value, t.sub_name, t.identifier])
                continue

            g = await self.bot.fetch_guild(db_guild.discord_id)
            channel = self.bot.get_channel(t.channel.discord_id)
            guild_name = g.name
            mat.append([guild_name, "None" if channel is None else channel.name, t.sale_type.value, t.sub_name, t.identifier])

        message.message = GetMatPrintString(mat)
        await message.Print()
        # 4) Update database -- No action required
        return State.SUCCESS

# ----------------------------------------------------------- #
#                     General Commands                        #
# ----------------------------------------------------------- #

    @commands.group(case_insensitive=True)  # noqa
    @emojiResponse
    async def shop(self, ctx):
        '''
        Catch-all for all shop commands.
        '''
        assert ctx.invoked_subcommand is not None, "A valid subcommand is needed."
        return

    @commands.command()
    @emojiResponse
    async def donate(self, ctx):
        '''
        Get details about how to donate to help keep the bot running.
        '''
        # Check if we should update the price estimate
        timeSincePriceUpdate = datetime.now() - self.donateLastUpdated
        if timeSincePriceUpdate.days > 2:  # Only update once per 2 days max
            self.donateLastUpdated = datetime.now()
            self.botMonthlyPrice = GetBotCost()

        botCreate = datetime(2019, 5, 11)  # May 11, 2019 is the first commit for the bot.
        botAge = (datetime.today() - botCreate)
        months = int(round(botAge / timedelta(weeks=4), 0))
        message = "This bot takes a lot of time and work to develop, update and maintain! I've worked on this bot for " + str(months)
        message += " months now, and I'll keep going as long as there is a demand.\n If you would like to help support the bot,"
        message += " feel free to donate using the link below. It helps pay the monthly costs of the bot, and it helps encourage"
        message += " me to develop more features!\n\n"
        message += "current monthly cost: $" + str(round(self.botMonthlyPrice, 2)) + "\n"
        message += "Link to paypal: http://paypal.me/WhiteEleBot"

        await PrintTo(ctx, message, raw=True)
        return State.SUCCESS

# ----------------------------------------------------------- #
#                       Owner Commands                        #
# ----------------------------------------------------------- #

    @shop.command()
    @commands.is_owner()
    async def announce(self, ctx, *, message):
        '''
        Sends a message to all shops that use this bot.
        '''
        message = "\n".join(message.split("\\n"))
        with session_scope(self.db) as session:
            guilds = session.query(Guild).all()
            for guild in guilds:
                if guild.backend_channel_discord_id is None:
                    continue  # Ignore this server, we don't know where to send messages to
                if guild.seller_role_discord_id is None:
                    role_mention = "To whom it may concern,"
                else:
                    role_mention = self.bot.get_guild(guild.discord_id).get_role(guild.seller_role_discord_id).mention

                backend = self.bot.get_channel(guild.backend_channel_discord_id)
                await PrintTo(backend, role_mention + "\n" + message, raw=True)
        await ctx.message.add_reaction(u"\u2705")  # heavy white check mark

    @shop.command()
    @commands.is_owner()
    @emojiResponse
    async def blacklist(self, ctx, guild_id=None):
        '''
        Blacklist servers for use of the shop.

        Arguments:
        guild_id: Must match the discord ID of a guild the bot is currently in.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord
        if guild_id is None:
            guild_id = ctx.guild.id

        valid_id = False
        for guild in self.bot.guilds:
            if guild_id == guild.id:
                valid_id = True

        assert valid_id, "This ID does not match any known guilds this bot is on."

        # 2) Query the database
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=guild_id).one_or_none()
            assert guild_entry is not None, "This guild is not in the database."
            whitelisted = guild_entry.whitelisted

        # 3) Update discord
        assert whitelisted, "This guild is already blacklisted."

        message.message = "Blacklisting the guild."
        await message.Print()

        # 4) Update database
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=guild_id).first()
            guild_entry.whitelisted = False

        return State.SUCCESS

    @shop.command()
    @commands.is_owner()
    @emojiResponse
    async def whitelist(self, ctx, guild_id=None):
        '''
        Whitelist servers for use of the shop.

        Arguments:
        guild_id: Must match the discord ID of a guild the bot is currently in.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord
        if guild_id is None:
            guild_id = ctx.guild.id

        valid_id = False
        for guild in self.bot.guilds:
            if guild_id == guild.id:
                valid_id = True

        assert valid_id, "This ID does not match any known guilds this bot is on."

        # 2) Query the database
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=guild_id).first()
            if guild_entry is None:
                whitelisted = None
            else:
                whitelisted = guild_entry.whitelisted

        # 3) Update discord
        assert not whitelisted, "This guild is already whitelisted."

        message.message = "Whitelisting the guild."
        await message.Print()

        # 4) Update database
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=guild_id).first()
            if whitelisted is None:
                guild = Guild(discord_id=guild_id, whitelisted=True)
                session.add(guild)
            else:
                guild_entry.whitelisted = True

        return State.SUCCESS

    @shop.command()
    @commands.is_owner()
    @emojiResponse
    async def servers(self, ctx):
        '''
        Prints out the current guilds and their info.
        '''
        # Parse from discord -- Nothing to do
        # Update/query Database
        mat = [['Guild', 'Shop Name', 'Seller role', 'backend channel', 'whitelisted']]
        with session_scope(self.db, query=True) as session:
            guilds = session.query(Guild).all()
            for guild in guilds:
                # Check for if we still have access to this server
                whitelisted = guild.whitelisted
                if guild.discord_id not in [discord_guild.id for discord_guild in self.bot.guilds]:
                    guild_name = guild.discord_id
                    shop_name = "None" if guild.category_discord_id else guild.category_discord_id
                    role_name = "None" if guild.seller_role_discord_id else guild.seller_role_discord_id
                    backend_channel_name = "None" if guild.backend_channel_discord_id is None else guild.backend_channel_discord_id
                else:
                    g = await self.bot.fetch_guild(guild.discord_id)
                    guild_name = g.name

                    shop_category = self.bot.get_channel(id=guild.category_discord_id)
                    shop_name = "None" if shop_category is None else shop_category.name

                    role = discord.utils.get(g.roles, id=guild.seller_role_discord_id)
                    role_name = "None" if role is None else role.name

                    backend_channel = self.bot.get_channel(id=guild.backend_channel_discord_id)
                    backend_channel_name = "None" if backend_channel is None else backend_channel.name

                mat.append([guild_name, shop_name, role_name, backend_channel_name, whitelisted])

        if len(mat) == 1:  # Just the header
            await PrintTo(ctx, "No guild entries found.")
            return State.WARNING

        await PrintTo(ctx, GetMatPrintString(mat))
        return State.SUCCESS

# ----------------------------------------------------------- #
#                        Admin Commands                       #
# ----------------------------------------------------------- #

    @commands.check(IsWhitelisted)
    @commands.has_permissions(administrator=True)
    @shop.command()
    @emojiResponse
    async def info(self, ctx):
        '''
        Displays general info about the shop, in summary form.
        '''
        message = Message(ctx)
        message.raw = True

        message_lines = []
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).one()

            # Guild settings
            category = await CICategory().convert(ctx, str(guild_entry.category_discord_id))
            seller_role = await CIRole().convert(ctx, str(guild_entry.seller_role_discord_id))
            backend_channel = await CIChannel().convert(ctx, str(guild_entry.backend_channel_discord_id))

            category_name = category.name if category is not None else "Not set up"
            message_lines.append(f"Shop Category: {category_name}")

            if isinstance(seller_role, str):
                seller_role_name = "Not set up"
            elif seller_role == ctx.guild.default_role:
                seller_role_name = "everyone"
            else:
                seller_role_name = seller_role.name
            message_lines.append(f"Seller role: {seller_role_name}")
            message_lines.append(f"Backend channel: {backend_channel.mention}")
            message_lines.append(f"autoincrement: {guild_entry.autoincrement}")
            message_lines.append(f"Public: {guild_entry.public}")
            message_lines.append("")

            # Summary of types
            message_lines.append(f"Total number of sale types: {len(guild_entry.types)}")
            message_lines.append("Types:")
            for t in guild_entry.types:
                channel = self.bot.get_channel(id=t.channel.discord_id)
                message_lines.append(f"{t.sale_type.value}: {t.sub_name} ({t.identifier}) -> {channel.mention}")
            message_lines.append("")

            # Summary of sales
            total_for_sale_num = 0
            total_for_sale_gold = 0
            total_sold_num = 0
            total_sold_gold = 0
            total_reserved_num = 0
            total_reserved_gold = 0
            total_cancelled_num = 0
            for t in guild_entry.types:
                for sku in t.skus:
                    total_for_sale_num += sku.quantity_remaining
                    price = sku.price_per
                    if price == -1:
                        price = sku.versions[0].price_per
                    total_for_sale_gold += sku.quantity_remaining * price
                    for sC in sku.state_changes:
                        if sC.transaction_type == Transactions.FINALIZE:
                            total_sold_num += sC.purchase_amount
                            total_sold_gold += sC.purchase_amount * price
                        elif sC.transaction_type == Transactions.RESERVATION:
                            total_reserved_num += sC.reserve_number
                            total_reserved_gold += sC.reserve_number * price
                        elif sC.transaction_type == Transactions.UNRESERVATION:
                            total_reserved_num -= sC.unreserve_number
                            total_reserved_gold -= sC.unreserve_number * price
                        elif sC.transaction_type == Transactions.CANCELLATION:
                            total_cancelled_num += 1
            message_lines.append(f"Total currently for sale: {total_for_sale_num} ({total_for_sale_gold}g)")
            message_lines.append(f"Total reserved: {total_reserved_num} ({total_reserved_gold}g)")
            message_lines.append(f"Total sold: {total_sold_num} ({total_sold_gold}g)")
            message_lines.append(f"Total cancelled: {total_cancelled_num}")

        message.message = "\n".join(message_lines)
        await message.Print()
        return State.SUCCESS

    @commands.check(IsWhitelisted)
    @commands.has_permissions(administrator=True)
    @shop.group()
    @emojiResponse
    async def setup(self, ctx):
        '''
        Group of commands used to set up a shop on the server.

        Can also be used to edit the configuration of a shop on this server.
        If this command is called with no subcommands, it will do a "quick setup" and fill
        in any remaining gaps in the setup process.
        '''
        if ctx.invoked_subcommand is None:
            message = Message(ctx)
            message.raw = True
            assert len(ctx.message.content.split(" ")) == 2, "Unrecognized subcommand."
            message.message = "Setup called with no subcommand - automatically creating any missing channels/data"
            await message.Print()
            content = []

            with session_scope(self.db, query=True) as session:
                guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).first()

            if guild_entry.category_discord_id is None:
                await self.category.callback(self, ctx)
            else:
                content.append('• Shop Category is already set up.')

            if guild_entry.seller_role_discord_id is None:
                await self.role.callback(self, ctx)
            else:
                content.append('• Seller role is already set up.')

            if guild_entry.backend_channel_discord_id is None:
                await self.backend.callback(self, ctx)
            else:
                content.append('• Backend channel is already set up.')

            message.message = "\n".join(content)
            await message.Print()
            await self.info.callback(self, ctx)
            return State.SUCCESS
        return

    @setup.command()
    @commands.bot_has_guild_permissions(manage_roles=True, manage_channels=True)
    @emojiResponse
    async def role(self, ctx, seller_role: CIRole = None):
        '''
        Tell the bot which role is allowed to sell in the shop.

        If this is @everyone, only the person who posted the sale
        will be able to edit it (or an admin).
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord
        if seller_role is None:
            seller_role = ctx.guild.default_role

        # 2) Query the database
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).first()
            db_role_discord_id = guild_entry.seller_role_discord_id

            # 3) Update discord
            if type(seller_role) == str:  # Create a new one with this name
                message.message = "Creating a new role for sellers."
                await message.Print()
                seller_role = await ctx.guild.create_role(name=seller_role, mentionable=True)

            assert db_role_discord_id != seller_role.id, "This role is already the seller role."
            guild_entry.seller_role_discord_id = seller_role.id

            if seller_role == ctx.guild.default_role:
                message.message = "Warning: Anyone in this server can sell/reserve/unreserve/cancel/etc. *any* items/animals in this shop."
                await message.Print()
            # If seller_role_discord_id is already set, update it
            if db_role_discord_id is not None:
                seller_role_name = seller_role.name if seller_role != ctx.guild.default_role else "everyone"  # avoid @everyone mention
                message.message = "Updating seller role to " + seller_role_name
                await message.Print()

            # If the backend channel is set up, update its permissions
            if guild_entry.backend_channel_discord_id is not None:
                print("updating backend permissions")
                backend_channel = self.bot.get_channel(id=guild_entry.backend_channel_discord_id)
                print('awaiting')
                await SetBackendPermissions(ctx, backend_channel, guild_entry)

            # 4) Update database
            print('updating')
        return State.SUCCESS

    @setup.command()
    @commands.bot_has_guild_permissions(manage_roles=True, manage_channels=True)
    @emojiResponse
    async def category(self, ctx, category_name=None):
        '''
        Tell the bot which category the shop should be put in.

        The bot must have an auto-assigned role for this command - this should happen automatically when
        you invite the bot to your server. If it doesn't, you'll need to re-invite the bot to your server
        and give it all of the permissions it needs.
        '''
        message = Message(ctx)
        message.raw = True
        prioritizeDB = False
        # 1) Accept and parse input from discord
        if category_name is None:
            category_name = "ATLAS Shop"
            prioritizeDB = True
        category = await CICategory().convert(ctx, category_name)

        # Recover the role assigned to the bot
        botRole = next((role for role in ctx.guild.me.roles if role.managed), None)
        assert botRole is not None, "The bot must have an auto-assigned role for permissions. Please re-invite the bot to your server and give it the requested permissions."

        bot_perms = PermissionOverwrite(send_messages=True, add_reactions=True)
        all_perms = PermissionOverwrite(send_messages=False, add_reactions=False, read_messages=True)

        overwrites = {
            ctx.guild.default_role: all_perms,
            botRole: bot_perms
        }

        # 2) Query the database
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).options(subqueryload(Guild.channels)).filter_by(discord_id=ctx.guild.id).first()

            # 3) Update discord
            if not isinstance(category, discord.CategoryChannel):
                if guild_entry.category_discord_id is not None:
                    if prioritizeDB:
                        message.message = "Shop category is already set up -- ignoring command."
                        await message.Print()
                        return State.WARNING

                    # Here we have a name for a new (currently nonexisting) category, but one
                    # currently exists in the db -- update the name only
                    discord_category = self.bot.get_channel(guild_entry.category_discord_id)

                    await discord_category.edit(name=category, overwrites=overwrites)
                    message.message = f"Updated the name of the existing shop category to {category}."
                    await message.Print()
                    return State.SUCCESS

                # Here, we have a name for a new (currently nonexisting) category, and
                # no record exists in the db -- make a new category and set up permissions/channels
                message.message = f"Creating a new category called {category_name}."
                await message.Print()

                category = await ctx.guild.create_category(category_name.upper())
            elif category.id == guild_entry.category_discord_id:
                message.message = category.name + " is already the shop category."
                await message.Print()
                return State.WARNING

            # Now category is an existing discord category, and it's different than the one
            # in the db -- hook this category into the db and move any relevant existing channels in it
            print("updating category perms")
            await category.edit(overwrites=overwrites)

            message.message = f"Updating the shop category to \"{category.name}\"."
            await message.Print()
            guild_entry.category_discord_id = category.id

            # Move any existing sales/backend channels into the category
            for channel_entry in guild_entry.channels:
                if channel_entry.deleted:
                    continue
                channel = self.bot.get_channel(id=channel_entry.discord_id)
                if channel.category != category:
                    await channel.edit(category=category, sync_permissions=True)

            # Move the backend channel into the category, if it exists
            if guild_entry.backend_channel_discord_id is not None:
                backend_channel = self.bot.get_channel(id=guild_entry.backend_channel_discord_id)
                if backend_channel.category != category:
                    await backend_channel.edit(category=category)
                    await SetBackendPermissions(ctx, backend_channel, guild_entry)

            # Make a reserve channel if one doesn't exist
            reserveChannel = None
            for cat_channel in category.text_channels:
                if "reserve-here" in cat_channel.name.lower():
                    reserveChannel = cat_channel
                    break

            if reserveChannel is None:
                reserveChannel = await category.create_text_channel(u"\U0001F536 - Reserve Here")  # Large orange diamond

            # In any case, update the reserve channel permissions
            all_perms = PermissionOverwrite(send_messages=True, add_reactions=True)

            overwrites = {
                ctx.guild.default_role: all_perms
            }

            await reserveChannel.edit(position=0, overwrites=overwrites)

            # 4) Update database

        return State.SUCCESS

    @setup.command()
    @commands.bot_has_guild_permissions(manage_roles=True, manage_channels=True)
    @emojiResponse
    async def backend(self, ctx, channel: CIChannel = None):
        '''
        Creates a backend channel with the given name.

        If no name is given, it will call the channel "backend-sell-here".
        You must use `-shop setup category` before using this command.
        '''
        message = Message(ctx)
        message.raw = True
        prioritizeDB = False
        # 1) Accept and parse input from discord
        if channel is None:
            prioritizeDB = True
            channel = await CIChannel().convert(ctx, "backend-sell-here")

        # 2) Query the database
        message_lines = []
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).first()

            # 3) Update discord
            category = await CICategory().convert(ctx, str(guild_entry.category_discord_id))
            assert isinstance(category, discord.CategoryChannel), "You must set up a category first, using `-shop setup category`."

            if not isinstance(channel, discord.TextChannel):
                if guild_entry.backend_channel_discord_id is not None:
                    if prioritizeDB:
                        message.message = "Backend channel is already set up -- ignoring command."
                        await message.Print()
                        return State.WARNING

                    # Update the name only
                    discord_channel = self.bot.get_channel(guild_entry.backend_channel_discord_id)
                    await discord_channel.edit(name=channel)
                    message.message = "Updated the name of the existing backend channel."
                    await message.Print()
                    return State.SUCCESS

                # Create the channel - sellers and the bot can see it, but regular members can't.
                message_lines.append("Creating a new backend channel")
                channel = await category.create_text_channel(channel)
                await SetBackendPermissions(ctx, channel, guild_entry)

                # channel = await category.create_text_channel(channel, overwrites=overwrites)
            else:
                await SetBackendPermissions(ctx, channel, guild_entry)
                if channel.id == guild_entry.backend_channel_discord_id:
                    message_lines.append(channel.mention + " is already configured as the backend channel.")
                    await message.Print()
                    return State.WARNING

            message_lines.append("Updating the backend channel to " + channel.mention + ".")
            await message.Print()

            if channel.category != category:
                await channel.edit(category=category)

            # 4) Update database
            guild_entry.backend_channel_discord_id = channel.id

        message.message = "\n".join(message_lines)
        await message.Print()
        return State.SUCCESS

    @setup.command()
    @emojiResponse
    async def autoincrement(self, ctx, autoincrement):
        '''
        Change the autoincrement setting of the shop.

        When True, the shop will automatically determine the SKU number for sales.
        When False, you have to submit a SKU id (e.g. EL10) when selling an animal.

        Arguments:
        autoincrement: true or false
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord
        assert autoincrement.lower() in ['true', 'false'], f"Autoincrement setting \"{autoincrement}\" is invalid."
        if autoincrement.lower() == 'true':
            autoincrement = True
        else:
            autoincrement = False

        # 2) Query the database
        message_lines = []
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).one()
            assert guild_entry.autoincrement != autoincrement, f"This guild already has autoincrement set to {autoincrement}."
            guild_entry.autoincrement = autoincrement

            message_lines.append(f"Setting autoincrement setting to {autoincrement}.")

        message.message = "\n".join(message_lines)
        await message.Print()
        return State.SUCCESS

    @setup.command()
    @emojiResponse
    async def public(self, ctx, public):
        '''
        Change the public setting of the shop.

        When True, the shop will let anyone post sales, but only the seller role, admins, and the seller themselves can edit posts.
        When False, only admins and members with the seller role can sell or edit posts.

        Arguments:
        public: true or false
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord
        assert public.lower() in ['true', 'false'], f"Public setting \"{public}\" is invalid."
        if public.lower() == 'true':
            public = True
        else:
            public = False

        # 2) Query the database
        message_lines = []
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).one()
            assert guild_entry.public != public, f"This guild already has public set to {public}."
            guild_entry.public = public

            message_lines.append(f"Setting public setting to {public}.")

            # If there is a backend channel, update its permissions to be viewable or not by everyone
            if guild_entry.backend_channel_discord_id is not None:
                backend_channel = self.bot.get_channel(id=guild_entry.backend_channel_discord_id)
                await SetBackendPermissions(ctx, backend_channel, guild_entry)

        message.message = "\n".join(message_lines)
        await message.Print()
        return State.SUCCESS

    async def CreateOrUpdateType(self, ctx, sale_type, sub_name, identifier, channel: CIChannel = None):
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord
        assert identifier.isalpha() and len(identifier) <= 8, f"{identifier} is an invalid identifier."
        identifier = identifier.upper()
        sub_name = sub_name.lower().replace(' ', '_')
        # underscore_separated_words
        assert re.fullmatch(r'([a-z]+_?)+', sub_name) is not None, f"Invalid {sale_type.value} name."
        sub_name = sub_name[0].upper() + sub_name[1:].lower()

        if channel is None:
            channel = sub_name

        if isinstance(channel, str):
            channel = await CIChannel().convert(ctx, channel)

        # 2) Query the database - get the type (by identifier or by name) and a channel
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).first()
            type_entry = session.query(Type).filter_by(sale_type=sale_type, sub_name=sub_name).options(subqueryload(Type.channel)).filter_by(guild_id=guild_entry.id).first()
            type_by_identifier = session.query(Type).filter_by(identifier=identifier).options(subqueryload(Type.channel)).filter_by(guild_id=guild_entry.id).first()

            if type_by_identifier is not None and type_entry != type_by_identifier:
                message.message = f"{identifier} is already used by {type_by_identifier.sale_type.value} type \"{type_by_identifier.sub_name}\"."
                await message.Print()
                return State.WARNING

            # See if we have a channel
            if isinstance(channel, discord.TextChannel):
                channel_entry = session.query(Channel).options(subqueryload(Channel.types)).filter_by(discord_id=channel.id).first()
            else:
                channel_entry = None

            # Check for the rare case where the channel is deleted from discord without the bot's knowledge
            if channel_entry is not None and not isinstance(channel, discord.TextChannel):
                message.message = f"Discord channel {channel} does not exist, but is in the database - this is likely a bug. Ask an admin for help."
                await message.Print()
                return State.FAILURE

            # Check for the input matching discord/db state exactly
            exact_match = True
            if type_entry is None:
                exact_match = False
            elif type_by_identifier != type_entry:
                exact_match = False
            elif type_entry.channel != channel_entry:
                exact_match = False

            if exact_match:
                message.message = "Arguments match exactly with an existing type - no action performed."
                await message.Print()
                return State.WARNING

            # 3) Update discord
            tasks = []

            if not isinstance(channel, discord.TextChannel):
                category = self.bot.get_channel(guild_entry.category_discord_id)
                channel = await category.create_text_channel(name=channel)
                tasks.append(f"Creating a new discord channel, {channel.mention}.")
            else:
                tasks.append(f"Hooking into existing discord channel, {channel.mention}.")

            # message.message = f"type: {type_entry}"
            # await message.Print()
            if type_entry is not None:
                for sku in type_entry.skus:
                    if sku.quantity_remaining > 0 or len(sku.CreateCollapsedReservationList(self.bot)) > 0:
                        discord_message = None
                        try:
                            oldChannel = self.bot.get_channel(type_entry.channel.discord_id)
                            discord_message = await oldChannel.fetch_message(sku.listing_discord_id)
                        except Exception:
                            message.message = f"This {sku.type.sale_type.value} {sku.sku_id} has no message to remove - this is a bug."
                            await message.Print()

                        if discord_message is not None:
                            skuEmbed = SkuEmbed.from_message(discord_message, self.bot, sku)
                            await discord_message.delete()
                            listing = await channel.send(embed=skuEmbed.embed)
                            sku.listing_discord_id = listing.id

            # 4) Update database

            # Check if we need to add a channel entry
            if channel_entry is None:
                channel_entry = Channel(guild_id=guild_entry.id, discord_id=channel.id)
                guild_entry.channels.append(channel_entry)
                tasks.append("This channel has never been used by the shop before.")
                session.commit()  # To get a channel_entry.id value

            # Create a type if none are found with this name
            guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).first()
            if type_entry is None:
                type_entry = Type(guild_id=guild_entry.id, sale_type=sale_type, sub_name=sub_name, identifier=identifier, channel_id=channel_entry.id)
                channel_entry.types.append(type_entry)
                tasks.append(f"Creating a new {type_entry.sale_type.value} type: {type_entry.sub_name} ({type_entry.identifier}) in channel {channel.mention}.")
            else:
                type_entry.channel_id = channel_entry.id
                type_entry.identifier = identifier
                tasks.append(f"Updating existing {type_entry.sale_type.value} type: {type_entry.sub_name} ({type_entry.identifier}) in channel {channel.mention}.")
        message.message = "\n".join(tasks)
        await message.Print()

        return State.SUCCESS

    @setup.command()
    @emojiResponse
    async def animal(self, ctx, breed, identifier, channel=None):
        '''
        Registers an animal to be sold in a given channel.

        Arguments
        ---------
        breed: If you want to use a space, wrap the whole name in quotes. You can also use
            underscores instead of spaces.
        identifier: Up to 8 letters, no numbers or special characters.
        channel: Text Channel used to sell this animal in. If not included, a new channel will be made.
        '''
        return await self.CreateOrUpdateType(ctx, SaleType.ANIMAL, breed, identifier, channel)

    @setup.command()
    @emojiResponse
    async def item(self, ctx, item_name, identifier, channel=None):
        '''
        Registers a type of item to be sold in a specific channel.

        Arguments
        ---------
        item_name: Name of an item. Spaces and underscores are allowed, but no numbers.
        identifier: Alphabetic string up to 8 letters long
        channel: Text Channel used to sell this animal in. If not included, a new channel will be made.
        '''
        return await self.CreateOrUpdateType(ctx, SaleType.ITEM, item_name, identifier, channel)

# ----------------------------------------------------------- #
#                      Show some examples                     #
# ----------------------------------------------------------- #

    @commands.check(IsWhitelisted)
    @shop.group()
    @emojiResponse
    async def example(self, ctx):
        '''
        Show examples for how to use various parts of the shop
        '''
        assert ctx.invoked_subcommand is not None, "A valid subcommand is needed."
        return

    @example.command(name="sell")
    @emojiResponse
    async def example_sell(self, ctx):
        '''
        Shows examples of how to sell things using the bot.

        Changes examples based on whether this guild is set up with autoincrement or not.
        '''
        with session_scope(self.db, query=True) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).one()
            autoincrement = guild_entry.autoincrement

        if autoincrement:
            await ctx.send(
                "NOTE: Set up all of the types first, to get identifiers (e.g. Elephant -> ELE)\n\n"
                "Selling animals:\n"
                "----------------\n"
                "**Level 26 male elephant, breedable, for 3500g:**\n"
                "-shop sell elephant male 26 3500g breedable\n"
                "\n"
                "**Level 44 female tiger, spayed, for 3000g. Also include a note saying where to get it:**\n"
                "-shop sell tiger 44 spayed female 3000g\n"
                "`<Shop bot says \"Tiger TI2 posted to #tigers\" for example>`\n"
                "-shop note add TI2 Pick up in A7\n"
                "\n"
                "**Level 35 crab for 5000g:**\n"
                "-shop sell crab 35 none false 5000g\n"
                "`<none is for gender (no gender) and false is for not breedable (neutered/spayed also work)>`\n"
                "\n"
                "Selling Items\n"
                "-------------\n"
                "**100 masterwork hatchets with 155% durability, for 250g each:**\n"
                "-shop sell hatchet #100 250g masterwork blueprint\n"
                "`<Shop bot sats \"Hatchet HTC1 posted to #items\" for example>`\n"
                "-shop note add HTC1 155% durability\n"
                "\n"
                "**A mythical large shipyard blueprint, with guaranteed perfects and with 7 crafts, for 50,000g:**\n"
                "-shop sell large_shipyard mythical 50000g\n"
                "`<Shop bot says \"Large Shipyard LGSHYD15 posted to #docks\" for example>`\n"
                "-shop note add LGSHYD15 7 crafts\n"
                "-shop note add LGSHYD15 Perfect crafts guaranteed\n"
            )
        else:
            await ctx.send(
                "NOTE: Set up all of the types first, to get identifiers (e.g. Elephant -> ELE)\n\n"
                "Selling animals:\n"
                "----------------\n"
                "**Level 26 male elephant, breedable, for 3500g, posted as ELE10:**\n"
                "-shop sell ELE10 male 26 3500g breedable\n"
                "\n"
                "**Level 44 female tiger, spayed, for 3000g, posted as TIG2. Also include a note saying where to get it:**\n"
                "-shop sell TIG2 44 spayed female 3000g\n"
                "`<Shop bot says \"Tiger TIG2 posted to #tigers\" for example>`\n"
                "-shop note add TIG2 Pick up in A7\n"
                "\n"
                "**Level 35 crab for 5000g, posted as CRAB23:**\n"
                "-shop sell CRAB23 35 none false 5000g\n"
                "`<none is for gender (no gender) and false is for not breedable (neutered/spayed also work)>`\n"
                "\n"
                "Selling Items\n"
                "-------------\n"
                "**100 masterwork hatchets with 155% durability, for 250g each, posted as AXE141:**\n"
                "-shop sell AXE141 #100 250g masterwork blueprint\n"
                "`<Shop bot sats \"Hatchet AXE141 posted to #items\" for example>`\n"
                "-shop note add AXE141 155% durability\n"
                "\n"
                "**A mythical large shipyard blueprint, with guaranteed perfects and with 7 crafts, for 50,000g (posted as LGSHYD15):**\n"
                "-shop sell LGSHYD15 mythical 50000g\n"
                "`<Shop bot says \"Large Shipyard LGSHYD15 posted to #docks\" for example>`\n"
                "-shop note add LGSHYD15 7 crafts\n"
                "-shop note add LGSHYD15 Perfect crafts guaranteed\n"
            )
        return State.SUCCESS

    @example.command(name="reserve")
    @emojiResponse
    async def example_reserve(self, ctx):
        '''
        Shows examples of how to reserve things using the bot.
        '''
        await ctx.send(
            "Reserving a sale item:\n"
            "----------------------\n"
            r"**Reserve 5 hatchets (ID HTC20) for \@Someone**\n"
            r"-shop reserve HTC20 #5 \@Someone\n"
            "**Reserve one bear (ID BR1) for someone not in this server**\n"
            "-shop reserve BR1 anon\n"
            "`<anon, anonymous, and unknown all indicate someone not in this server>`"
        )
        return State.SUCCESS

# ----------------------------------------------------------- #
#                       Create new sales                      #
# ----------------------------------------------------------- #

    @commands.check(IsWhitelisted)
    @commands.check(CanSell)
    @commands.check(IsInBackend)
    @shop.group()
    @emojiResponse
    async def sell(self, ctx, type_name_or_id, *args):
        '''
        Create a new sale - can only be used in the backend channel.

        Arguments (other than type_name_or_id) are in any order, and they depend on what you're selling

        All sales
        ---------
        Required Arguments:
        - type_name_or_id : If autoincrement is on, this is just the sale type (e.g. Elephant or Pistol)
                       If autoincrement is off, this is the full ID for the item (e.g. EL10 or GUN20) - you have to
                       keep track of the identifier for each type yourself (you can find them using -shop info).
        - price     : Should have #####g format - price per unit.

        Optional Arguments:
        - quantity  : Should have format like "#5" or "#62". Defaults to "#1", must be smaller than 1000.

        Animals
        -------
        Required Arguments:
        - level     : Total current level for the animal.
        - gender    : Male, Female, or None
        - breedable : True/False/Breedable/Neutered/Spayed are accepted

        Items
        -----
        Optional Arguments:
        - rarity: Common, Fine, Journeyman, Masterwork, Legendary, or Mythical allowed.
            determines the color on the side of the post. C, F, JM, Ma, L, and My are also allowed.
        - blueprint: True, false, or blueprint are accepted.
        '''
        # pre-work
        self.Add_member_to_DB(ctx.guild, ctx.author)
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord - have to skip, to query the type first
        # 2) Query the database
        # Check to make sure sub_type is in the database
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(discord_id=ctx.guild.id).one()
            if guild_entry.autoincrement:
                type_name = type_name_or_id
                type_entry = session.query(Type).filter_by(sub_name=type_name, guild=guild_entry).one_or_none()
                assert type_entry is not None, "This sale type could not be found in the database - ask an administrator for help."
                sku_id = type_entry.identifier + str(type_entry.current_number)
                type_entry.current_number += 1
            else:
                sku_id = type_name_or_id.upper()
                assert re.match(r"[a-zA-Z]{0,8}\d+", sku_id), f"Sku ID {sku_id} is invalid."
                identifier = "".join([s for s in sku_id if s.isalpha()])
                number = int("".join([s for s in sku_id if not s.isalpha()]))
                type_entry = session.query(Type).filter_by(identifier=identifier, guild=guild_entry).one_or_none()
                assert type_entry is not None, "This sale type could not be found in the database - ask an administrator for help."
                # Check to ensure this sku isn't already present
                sku_entry = session.query(Sku).filter_by(sku_id=sku_id).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
                assert sku_entry is None, f"{sku_id} is already in the system. Try {identifier}{type_entry.current_number} or higher."
                type_entry.current_number = max(type_entry.current_number, number) + 1
            channel_entry = type_entry.channel
            seller_entry = session.query(Member).filter_by(discord_id=ctx.author.id).first()
            print("seller entry:", seller_entry)

            # Finish parsing
            aP = ArgParser(ctx)
            aP.optionalPatterns = {
                "quantity": re.compile(r"^#[0-9]+$")
            }
            if type_entry.sale_type == SaleType.ITEM:
                aP.requiredPatterns = {
                    "price": re.compile(r"^[0-9]+g$", re.I),
                }
                rarity_strings = [s for val in Rarity for s in val.strings]
                aP.optionalPatterns.update({
                    "rarity": re.compile("^(" + "|".join(rarity_strings) + ")$", re.I),
                    "blueprint": re.compile(r"^true|false|blueprint$", re.I)
                })
            else:
                aP.requiredPatterns = {
                    "level": re.compile(r"^[0-9]+$"),
                    "price": re.compile(r"^[0-9]+g$"),
                    "gender": re.compile(r"^(male|female|none)$", re.I),
                    # Breed - check from types
                    "breedable": re.compile("^(" + '|'.join(BREEDABLE_OPTIONS + NON_BREEDABLE_OPTIONS) + ")$", re.I)
                }

            print("Parsing args")
            parsedArgs = await aP.Parse(args)
            remainingArgs = [arg for arg in args if arg not in parsedArgs.values()]
            assert len(remainingArgs) == 0, "Too many arguments."

            # Change data types and remove extra characters
            parsedArgs['price'] = int(parsedArgs['price'][:-1])
            if parsedArgs['quantity'] is None:
                parsedArgs['quantity'] = 1
            else:
                parsedArgs['quantity'] = int(parsedArgs['quantity'][1:])
            assert parsedArgs['quantity'] < 1000, "Quantity is too high."
            print("Finished parsing")

            if type_entry.sale_type == SaleType.ANIMAL:
                parsedArgs['level'] = int(parsedArgs['level'])
                parsedArgs['gender'] = parsedArgs['gender'][0].upper() + parsedArgs['gender'][1:].lower()

                if parsedArgs['breedable'].lower() in BREEDABLE_OPTIONS:
                    parsedArgs['breedable'] = True
                else:
                    parsedArgs['breedable'] = False

                sale_entry = Animal(
                    sku_id=sku_id,
                    type=type_entry,
                    channel_id=channel_entry.id,
                    price_per=parsedArgs['price'],
                    quantity_remaining=parsedArgs['quantity'],
                    level=parsedArgs['level'],
                    gender=parsedArgs['gender'],
                    breedable=parsedArgs['breedable']
                )
            else:
                parsedArgs['rarity'] = Rarity.from_string(str(parsedArgs['rarity']))
                if parsedArgs['blueprint'] is None or parsedArgs['blueprint'].lower() == 'false':
                    parsedArgs['blueprint'] = False
                else:
                    parsedArgs['blueprint'] = True

                sale_entry = Item(
                    sku_id=sku_id,
                    type=type_entry,
                    channel_id=channel_entry.id,
                    price_per=parsedArgs['price'],
                    quantity_remaining=parsedArgs['quantity'],
                    rarity=parsedArgs['rarity'],
                    blueprint=parsedArgs['blueprint']
                )
            sale_entry.seller = seller_entry

            print("Creating posting")
            Posting(time=datetime.now(), staff=seller_entry, sku=sale_entry)

            # 3) Update discord
            print("doing discord updates")
            image = ctx.message.attachments[0].url if len(ctx.message.attachments) > 0 else None
            skuEmbed = SkuEmbed.create_new(self.bot, sale_entry, image)
            channel = self.bot.get_channel(channel_entry.discord_id)
            listing = await channel.send(embed=skuEmbed.embed)
            sale_entry.listing_discord_id = listing.id
            message.message = f"{type_entry.sub_name} {sale_entry.sku_id} posted to {channel.mention}."
            await message.Print()
            print("Trying to commit")

        # 4) Update database
        return State.SUCCESS

# ----------------------------------------------------------- #
#                         Remove sales                        #
# ----------------------------------------------------------- #

    @commands.check(IsWhitelisted)
    @commands.check(CanAlterSale)
    @shop.command(usage="<item id>")
    @emojiResponse
    async def cancel(self, ctx, sku):
        '''
        Cancels a sale item from the bot.

        Argument
        --------
        sku: The sku (ID) of the sale item. Can also be comma-separated IDs. E.G. "BR31", "ELE1,ELE2" or "SWORD01"
        '''
        message = Message(ctx)
        message.raw = True
        self.Add_member_to_DB(ctx.guild, ctx.author)
        # 1) Accept and parse input from discord - need to access db first
        # 2) Query the database
        sku_list = sku.split(",")
        returnState = State.SUCCESS
        for sku in sku_list:
            message_lines = []
            with session_scope(self.db) as session:
                sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).first()
                assert sku_entry is not None, "This sale item is not in the database."
                staff_entry = session.query(Member).filter_by(discord_id=ctx.author.id).first()

                # Check if the item is for sale
                assert sku_entry.sale_state != SaleState.CANCELLED, f"{sku_entry.sku_id} has been cancelled by the seller."
                assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku_entry.sku_id} is sold out - it can no longer be cancelled."
                assert sku_entry.sale_state == SaleState.NO_RESERVATIONS, f"{sku_entry.sku_id} still has active reservations, so it can't be cancelled yet."

                # 3) Update discord
                returnState = State.SUCCESS
                channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
                assert channel is not None, "Channel in the database no longer exists. This is a bug."
                try:
                    discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
                    await discord_message.delete()
                    message_lines.append(f"Removing the listing for {sku_entry.sku_id} in {channel.mention}.")
                except Exception:
                    message.message = f"{sku_entry.sku_id} has no message to remove - this is a bug."
                    if returnState == State.SUCCESS:
                        returnState = State.WARNING

                # 4) Update database
                Cancellation(sku=sku_entry, staff=staff_entry, time=datetime.now())
                sku_entry.sale_state = SaleState.CANCELLED
                # sku_entry.transactions.append(cancellation)
                message_lines.append(f"\n{sku_entry.sku_id} has been cancelled.")
            message.message = "\n".join(message_lines)
            await message.Print()
        return returnState

# ----------------------------------------------------------- #
#                     Change sale status                      #
# ----------------------------------------------------------- #

    @commands.check(IsWhitelisted)
    @commands.check(CanAlterSale)
    @shop.command(usage="<item id> <reserve_for> [quantity]")
    @emojiResponse
    async def reserve(self, ctx, sku, *args):
        '''
        Reserve something that is currently for sale.

        Arguments
        ---------
        sku: The sku (ID) of the sale item. Can also be comma-separated IDs. E.G. "BR31", "ELE1,ELE2" or "SWORD01"
        reserve_for: mention/name of the person reserving the sale item.
            Use "anon", "anonymous", or "unknown" if they aren't in this guild.
        quantity: (optional) Number of sale items being reserved, formatted like "#5" or "#1". Defaults to 1.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord
        # TODO: Add the option for comma-separated skus
        assert len(args) <= 2, "Too many arguments."

        aP = ArgParser(ctx)
        aP.requiredPatterns = {
            "reserve_for": ConvertArg(CIMember)
        }
        aP.optionalPatterns = {
            "quantity": re.compile(r"^#[0-9]+$")
        }
        parsedArgs = await aP.Parse(args)
        reserve_for = parsedArgs['reserve_for']
        quantity = 1 if parsedArgs['quantity'] is None else int(parsedArgs['quantity'][1:])

        self.Add_member_to_DB(ctx.guild, ctx.author)
        if isinstance(reserve_for, discord.Member) or reserve_for is DefaultMember:
            self.Add_member_to_DB(ctx.guild, reserve_for)

        sku_list = sku.split(",")
        returnState = State.SUCCESS
        # 2) Query the database
        for sku in sku_list:
            message_lines = []
            with session_scope(self.db) as session:
                sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).first()
                assert sku_entry is not None, "This sale item is not in the database."
                staff_entry = session.query(Member).filter_by(discord_id=ctx.author.id).first()
                reserver_entry = session.query(Member).filter_by(discord_id=reserve_for.id).first()
                assert reserver_entry is not None, "Default user is not seeded - contact the mod author, this is a bug."

                # Check if the item is for sale
                assert sku_entry.sale_state != SaleState.CANCELLED, f"{sku_entry.sku_id} has been cancelled by the seller."
                assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku_entry.sku_id} is sold out - it can no longer be reserved."
                assert sku_entry.sale_state != SaleState.FULLY_RESERVED, f"{sku_entry.sku_id} has no more reservations, so it can't be reserved any more."
                assert quantity <= sku_entry.quantity_remaining, f"Cannot reserve that quantity of {sku_entry.sku_id}. Please choose at most {sku_entry.quantity_remaining}."

                reservation = Reservation(sku=sku_entry, staff=staff_entry, reserver=reserver_entry, reserve_number=quantity, time=datetime.now())
                sku_entry.quantity_remaining -= reservation.reserve_number
                sku_entry.SetState()

                # 3) Update discord
                channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
                assert channel is not None, "Channel in the database no longer exists. This is a bug."

                try:
                    discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
                except Exception as e:
                    print(e)
                    await ctx.send(e)
                    message_lines.append(f"{sku_entry.sku_id} has no message to update - this is a bug.")
                    returnState = State.WARNING

                if discord_message is not None:
                    embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                    embed.SetQuantity()
                    embed.SetReservations()
                    if sku_entry.quantity_remaining == 0:
                        embed.AllReserved()
                    await discord_message.edit(embed=embed.embed)

                # 4) Update database
                message_lines.append(f"{sku_entry.sku_id} has been reserved for {reservation.reserver.get_name(self.bot)}.")
            if message_lines:
                message.message = "\n".join(message_lines)
                await message.Print()
        await self.search_reservations.callback(self, ctx, reserve_for.display_name)
        return returnState

    @commands.check(IsWhitelisted)
    @commands.check(CanAlterSale)
    @shop.command(usage="<sku> [member] [quantity]")
    @emojiResponse
    async def unreserve(self, ctx, sku, *args):
        '''
        Move a sale item's status from 'reserved' to 'for sale'.

        Arguments
        ---------
        sku: The sku (ID) of the sale item. E.G. BR31 or SWORD01
        unreserve_for: mention/name of the person unreserving the sale item.
            Use "anon", "anonymous", or "unknown" if they aren't in this guild.
        quantity: (optional) Number of sale items being unreserved, formatted like "#5" or "#1". Defaults to all items for this member.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord
        assert len(args) <= 2, "Too many arguments."

        aP = ArgParser(ctx)
        aP.requiredPatterns = {
            "unreserve_for": ConvertArg(CIMember)
        }
        aP.optionalPatterns = {
            "quantity": re.compile(r"^#[0-9]+$")
        }
        parsedArgs = await aP.Parse(args)
        unreserve_for = parsedArgs['unreserve_for']
        quantity = None if parsedArgs['quantity'] is None else int(parsedArgs['quantity'][1:])

        self.Add_member_to_DB(ctx.guild, ctx.author)
        if isinstance(unreserve_for, discord.Member):
            self.Add_member_to_DB(ctx.guild, unreserve_for)

        sku_list = sku.split(",")
        returnState = State.SUCCESS
        # 2) Query the database
        for sku in sku_list:
            message_lines = []
            with session_scope(self.db) as session:
                print("getting sku")
                sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).first()
                assert sku_entry is not None, "This sale item is not in the database."
                staff_entry = session.query(Member).filter_by(discord_id=ctx.author.id).first()

                # # Check if the item is for sale
                assert sku_entry.sale_state != SaleState.CANCELLED, f"{sku_entry.sku_id} has been cancelled by the seller."
                assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku_entry.sku_id} is sold out - it can no longer be unreserved."
                assert sku_entry.sale_state != SaleState.NO_RESERVATIONS, f"{sku_entry.sku_id} has no active reservations to unreserve."
                # cancel_entry = session.query(Cancellation).filter_by(sku=sku_entry).one_or_none()
                # assert cancel_entry is None, f"This {sku_entry.type.sale_type.value} has been cancelled by the seller."

                # If more than 1 person has reservations, require a member
                if unreserve_for is None:
                    # assert len(sku_entry.CreateCollapsedReservationList(self.bot)) == 1, f"You must include a member to unreserve for, since there are multiple reservations for {sku_entry.sku_id}."
                    assert sku_entry.sale_state == SaleState.ONE_RESERVATION, f"You must include a member to unreserve for, since there are multiple reservations for {sku_entry.sku_id}."

                    # Figure out which member has some reservations left
                    members_reserving = session.query(Member).join(Member.reservations).filter_by(sku=sku_entry).all()
                    unreserve_member_db = None
                    for member in members_reserving:
                        if member.get_total_reservations(sku_entry.id) > 0:
                            unreserve_member_db = member
                            break
                    assert unreserve_member_db is not None, f"One reservation found, but no members have > 0 total reservation for {sku}. This is a bug."
                else:
                    unreserve_member_db = session.query(Member).filter_by(discord_id=unreserve_for.id).join(Member.reservations).filter_by(sku=sku_entry).all()
                    assert unreserve_member_db, f"This member has no reservations for {sku}."
                    unreserve_member_db = unreserve_member_db[0]

                # automatically unreserve all for this member, if quantity is not given
                if quantity is None:
                    quantity = unreserve_member_db.get_total_reservations(sku_entry.id)
                    assert quantity > 0, f"Automatically determined a negative number ({quantity}) of {sku_entry.sku_id}s to unreserve. This is a bug."

                unreservation = UnReservation(sku=sku_entry, time=datetime.now(), unreserve_number=quantity, staff=staff_entry, unreserver=unreserve_member_db)
                sku_entry.quantity_remaining += unreservation.unreserve_number
                sku_entry.SetState()

                # 3) Update discord
                returnState = State.SUCCESS
                channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
                assert channel is not None, "Channel in the database no longer exists. This is a bug."
                discord_message = None
                try:
                    discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
                except Exception:
                    message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                    returnState = State.WARNING

                if discord_message is not None:
                    # embed = self.UpdateListingEmbed(discord_message, sku_entry, unreservation)
                    embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                    embed.SetQuantity()
                    embed.SetReservations()
                    embed.ForSale()
                    await discord_message.edit(embed=embed.embed)
                    message_lines.append(f"Unreserving the listing in {channel.mention}.")

                # 4) Update database
                # sku_entry.transactions.append(unreservation)
                message_lines.append(f"{sku_entry.sku_id} has been unreserved.")
            message.message = "\n".join(message_lines)
            await message.Print()
        await self.search_reservations.callback(self, ctx, unreserve_for.display_name)
        return returnState

    @commands.check(IsWhitelisted)
    @commands.check(CanAlterSale)
    @shop.command()
    @emojiResponse
    async def sold(self, ctx, sku, *args):
        '''
        Finalize the sale of a something currently reserved.

        Arguments
        ---------
        sku: The sku (ID) of the sale item. E.G. BR31 or SWORD01
        buyer: mention/name of the person buying the sale item.
            Use "anon", "anonymous", or "unknown" if they aren't in this guild.
        quantity: (optional) Number of sale items being sold, formatted like "#5" or "#1". Defaults to all reserved items.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Accept and parse input from discord
        assert len(args) <= 2, "Too many arguments."

        aP = ArgParser(ctx)
        aP.requiredPatterns = {
            "buyer": ConvertArg(CIMember)
        }
        aP.optionalPatterns = {
            "quantity": re.compile(r"^#[0-9]+$")
        }
        parsedArgs = await aP.Parse(args)
        buyer = parsedArgs['buyer']
        quantity = None if parsedArgs['quantity'] is None else int(parsedArgs['quantity'][1:])

        self.Add_member_to_DB(ctx.guild, ctx.author)
        if isinstance(buyer, discord.Member):
            self.Add_member_to_DB(ctx.guild, buyer)

        sku_list = sku.split(",")
        returnState = State.SUCCESS
        # 2) Query the database
        for sku in sku_list:
            message_lines = []
            with session_scope(self.db) as session:
                print("getting sku")
                sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).first()
                assert sku_entry is not None, "This sale item is not in the database."
                staff_entry = session.query(Member).filter_by(discord_id=ctx.author.id).first()

                # Check if the item is for sale
                cancel_entry = session.query(Cancellation).filter_by(sku=sku_entry).one_or_none()
                assert cancel_entry is None, f"This {sku_entry.type.sale_type.value} has been cancelled by the seller."

                # If more than 1 person has reservations, require a member
                if buyer is None:
                    assert len(sku_entry.CreateCollapsedReservationList(self.bot)) == 1, f"You must include a member to finalize for, since there are multiple reservations for {sku_entry.sku_id}."

                    # Figure out which member has some reservations left
                    members_reserving = session.query(Member).join(Member.reservations).filter_by(sku=sku_entry).all()
                    buyer_db = None
                    for member in members_reserving:
                        if member.get_total_reservations(sku_entry.id) > 0:
                            buyer_db = member
                            break
                    assert buyer_db is not None, f"One reservation found, but no members have > 0 total reservation for {sku}. This is a bug."
                else:
                    buyer_db = session.query(Member).filter_by(discord_id=buyer.id).join(Member.reservations).filter_by(sku=sku_entry).all()
                    assert buyer_db, f"This member has no reservations for {sku}."
                    buyer_db = buyer_db[0]

                # Determine quantity if not given
                max_allowed = buyer_db.get_total_reservations(sku_entry.id)
                if quantity is None:
                    quantity = max_allowed
                    assert quantity > 0, f"Automatically determined an invalid number ({quantity}) of reservations - this is a bug."
                assert quantity <= max_allowed, f"{buyer_db.get_name(self.bot)} only has {max_allowed} reserved for {sku} -- {quantity} is too large."

                finalization = Finalization(sku=sku_entry, time=datetime.now(), staff=staff_entry, buyer=buyer_db, purchase_amount=quantity)
                sku_entry.SetState()

                # 3) Update discord
                returnState = State.SUCCESS
                channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
                assert channel is not None, "Channel in the database no longer exists. This is a bug."

                # Figure out how to tell how many reservations are left, and if we should delete vs update the message :)
                discord_message = None
                try:
                    discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
                except Exception:
                    message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                    returnState = State.WARNING

                if discord_message is not None:
                    if sku_entry.sale_state == SaleState.SOLD_OUT:
                        await discord_message.delete()
                        message_lines.append(f"Removing the listing in {channel.mention}.")
                    else:
                        # if sku_entry.quantity_remaining > 0 or len(sku_entry.CreateCollapsedReservationList(self.bot)) > 0:
                        embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                        embed.SetReservations()
                        await discord_message.edit(embed=embed.embed)
                        message_lines.append(f"Updating the listing in {channel.mention}.")
                    # else:
                    #     await discord_message.delete()
                    #     message.message = f"Removing the listing in {channel.mention}."

                # 4) Update database
                message_lines.append(f"{buyer_db.get_name(self.bot)} has purchased {finalization.purchase_amount} {sku_entry.sku_id}")
            message.message = "\n".join(message_lines)
            await message.Print()
        await self.search_purchases.callback(self, ctx, buyer.display_name)
        return returnState

# ----------------------------------------------------------- #
#                      Sku-editing commands                   #
# ----------------------------------------------------------- #

    @shop.group()
    @commands.check(IsWhitelisted)
    @emojiResponse
    async def edit(self, ctx):
        '''
        Edit the sale properties of a given sale item.
        '''
        assert ctx.invoked_subcommand is not None, "A valid subcommand is needed."
        return

    @edit.command(name="price")
    @commands.check(CanAlterSale)
    @emojiResponse
    async def edit_price(self, ctx, sku, price):
        '''
        Set a new price for some sale item.

        Arguments
        ---------
        sku: ID for the sale item you want to edit.
        price: The new price for this sale item, formatted like #######g, or "reset" to return the price history to its original.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input
        print("parsing args")
        assert re.match(r"^[0-9]+g|reset$", price, re.I), f"Price \"{price}\" is invalid."
        if price.lower() == "reset":
            price = -1
        else:
            price = int(price[:-1])

        # 2) Query the database
        message_lines = []
        print("opening connection")
        returnState = State.SUCCESS
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot add a note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            assert sku_entry.sale_state == SaleState.NO_RESERVATIONS, "Only sale items with no reservations can have their details edited."
            assert price == -1 or sku_entry.price_per != price, f"This price is already set as {price}."
            assert price != -1 or sku_entry.price_per != price, f"This price is already reset to {sku_entry.versions[0].price_per}."
            print("assertions complete")
            sku_entry.price_per = price

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            discord_message = None
            print("updating discord")
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                returnState = State.WARNING

            if discord_message is not None:
                embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                embed.SetPrice()
                await discord_message.edit(embed=embed.embed)
                message_lines.append(f"Updating the listing in {channel.mention}.")

        message.message = "\n".join(message_lines)
        await message.Print()
        return returnState

    @edit.command(name="seller")
    @commands.check(CanAlterSale)
    @emojiResponse
    async def edit_seller(self, ctx, sku, seller: CIMember):
        '''
        Set a new seller for some sale item.

        Arguments
        ---------
        sku: ID for the sale item you want to edit.
        seller: The new person in charge of selling this sale item.

        WARNING: If this is on a public shop, you cannot regain ownership without the help
        of an admin or the new seller. Use this command at your own risk.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input
        self.Add_member_to_DB(ctx.guild, seller)
        # 2) Query the database
        message_lines = []
        returnState = State.SUCCESS
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot add a note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            assert sku_entry.sale_state == SaleState.NO_RESERVATIONS, "Only sale items with no reservations can have their details edited."
            seller_entry = session.query(Member).filter_by(discord_id=seller.id).one_or_none()
            assert sku_entry.seller != seller_entry, f"This member is already set as the seller for {sku_entry.sku_id}."
            sku_entry.seller = seller_entry

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                returnState = State.WARNING

            if discord_message is not None:
                embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                embed.ForSale()
                await discord_message.edit(embed=embed.embed)
                message_lines.append(f"Updating the listing in {channel.mention}.")

        message.message = "\n".join(message_lines)
        await message.Print()
        return returnState

    @edit.command(name="image")
    @commands.check(CanAlterSale)
    @emojiResponse
    async def edit_image(self, ctx, sku):
        '''
        Set a new image for some sale item.

        Attach the image to this message, and it will be put on the sale item listing.

        Argument
        --------
        sku: ID for the sale item you want to edit.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input
        assert len(ctx.message.attachments) == 1, "You must attach an image with this command."
        image = ctx.message.attachments[0].url

        # 2) Query the database
        message_lines = []
        returnState = State.SUCCESS
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot add a note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            assert sku_entry.sale_state == SaleState.NO_RESERVATIONS, "Only sale items with no reservations can have their details edited."

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                returnState = State.WARNING

            if discord_message is not None:
                embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                embed.embed.set_image(url=image)
                await discord_message.edit(embed=embed.embed)
                message_lines.append(f"Updating the listing in {channel.mention}.")

        message.message = "\n".join(message_lines)
        await message.Print()
        return returnState

    @edit.command(name="quantity")
    @commands.check(CanAlterSale)
    @emojiResponse
    async def edit_quantity(self, ctx, sku, quantity):
        '''
        Set a new quantity (remaining) for some sale item.

        NOTE: this will change the amount of sale items ready to sell, not the *total*.
        This means that any reserved or sold sale items don't count toward this number.

        Arguments
        ---------
        sku: ID for the sale item you want to edit.
        new_price: The new price for this sale item, formatted like #######g
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input
        assert re.match(r"^#[0-9]+$", quantity, re.I), f"Quantity \"{quantity}\" is invalid."
        quantity = int(quantity[1:])

        # 2) Query the database
        message_lines = []
        returnState = State.SUCCESS
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot add a note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            assert sku_entry.sale_state == SaleState.NO_RESERVATIONS, "Only sale items with no reservations can have their details edited."
            assert sku_entry.quantity_remaining != quantity, f"This quantity is already set as {quantity}."
            sku_entry.quantity_remaining = quantity

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                returnState = State.WARNING

            if discord_message is not None:
                embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                embed.SetQuantity()
                if quantity == 0:
                    embed.AllReserved()
                else:
                    embed.ForSale()  # In case quantity_remaining was 0.
                await discord_message.edit(embed=embed.embed)
                message_lines.append(f"Updating the listing in {channel.mention}.")

        message.message = "\n".join(message_lines)
        await message.Print()
        return returnState

    @edit.command(name="level")
    @commands.check(CanAlterSale)
    @emojiResponse
    async def edit_level(self, ctx, sku, level):
        '''
        Set a new level for some animal.

        Arguments
        ---------
        sku: ID for the animal you want to edit.
        level: The new level for this animal (just a number)
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input
        assert re.match(r"^[0-9]+$", level, re.I), f"Level \"{level}\" is invalid."
        level = int(level)

        # 2) Query the database
        message_lines = []
        returnState = State.SUCCESS
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_type == SaleType.ANIMAL, f"{sku_entry.sku_id} is not an animal, so it has no level."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot add a note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            assert sku_entry.sale_state == SaleState.NO_RESERVATIONS, "Only sale items with no reservations can have their details edited."
            assert sku_entry.level != level, f"This level is already set as {level}."
            sku_entry.level = level

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                returnState = State.WARNING

            if discord_message is not None:
                embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                embed.SetID()
                await discord_message.edit(embed=embed.embed)
                message_lines.append(f"Updating the listing in {channel.mention}.")

        message.message = "\n".join(message_lines)
        await message.Print()
        return returnState

    @edit.command(name="breedable")
    @commands.check(CanAlterSale)
    @emojiResponse
    async def edit_breedable(self, ctx, sku, breedable=None):
        '''
        Set a new breedable status for some animal.

        Arguments
        ---------
        sku: ID for the animal you want to edit.
        breedable: (optional) The new breedability for this animal. True/False/Breedable/Neutered/Spayed are accepted.
                              Defaults to the opposite of the current status for this animal.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input
        if breedable is not None:
            assert re.match(r"^(" + '|'.join(BREEDABLE_OPTIONS + NON_BREEDABLE_OPTIONS) + ")$", breedable, re.I), f"Breedable option \"{breedable}\" is invalid."
            if breedable in BREEDABLE_OPTIONS:
                breedable = True
            else:
                breedable = False

        # 2) Query the database
        message_lines = []
        returnState = State.SUCCESS
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_type == SaleType.ANIMAL, f"{sku_entry.sku_id} is not an animal, so it cannot be bred."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot add a note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            assert sku_entry.sale_state == SaleState.NO_RESERVATIONS, "Only sale items with no reservations can have their details edited."
            assert breedable is None or sku_entry.breedable != breedable, "This animal already has this breedability option set."
            if breedable is None:  # Toggle breedability
                sku_entry.breedable = not sku_entry.breedable
            else:
                sku_entry.breedable = breedable

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                returnState = State.WARNING

            if discord_message is not None:
                embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                embed.SetNotes()
                await discord_message.edit(embed=embed.embed)
                message_lines.append(f"Updating the listing in {channel.mention}.")

        message.message = "\n".join(message_lines)
        await message.Print()
        return returnState

    @edit.command(name="blueprint")
    @commands.check(CanAlterSale)
    @emojiResponse
    async def edit_blueprint(self, ctx, sku, blueprint=None):
        '''
        Change whether an item is a blueprint or not.

        Arguments
        ---------
        sku: ID for the item you want to edit.
        blueprint: (optional) The blueprint status this item. "True"/"Blueprint" are interpreted as a blueprint, "false" is not.
                              Defaults to the opposite of the current status for this item.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input
        print("parsing bp argument")
        if blueprint is not None:
            assert re.match(r"^true|false|blueprint$", blueprint, re.I), f"Blueprint option \"{str(blueprint)}\" is invalid."
            if blueprint.lower() == "false":
                blueprint = False
            else:
                blueprint = True

        # 2) Query the database
        message_lines = []
        returnState = State.SUCCESS
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_type == SaleType.ITEM, f"{sku_entry.sku_id} is not an item, so it can't be a blueprint."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot add a note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            assert sku_entry.sale_state == SaleState.NO_RESERVATIONS, "Only sale items with no reservations can have their details edited."
            assert blueprint is None or sku_entry.blueprint != blueprint, "This item already has this blueprint setting."
            if blueprint is None:  # Toggle breedability
                sku_entry.blueprint = not sku_entry.blueprint
            else:
                sku_entry.blueprint = blueprint
            print("New blueprint setting:", sku_entry.blueprint)

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                returnState = State.WARNING

            if discord_message is not None:
                embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                embed.SetID()
                await discord_message.edit(embed=embed.embed)
                message_lines.append(f"Updating the listing in {channel.mention}.")

        message.message = "\n".join(message_lines)
        await message.Print()
        return returnState

    @edit.command(name="rarity")
    @commands.check(CanAlterSale)
    @emojiResponse
    async def edit_rarity(self, ctx, sku, rarity):
        '''
        Change the rarity of this item.

        Arguments
        ---------
        sku: ID for the item you want to edit.
        rarity: Common, Fine, Journeyman, Masterwork, Legendary, or Mythical allowed.
            Determines the color on the side of the post. C, F, JM, Ma, L, and My are also allowed.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input
        rarity_strings = [s for val in Rarity for s in val.strings]
        assert re.match(r"^(" + "|".join(rarity_strings) + ")$", rarity, re.I), f"Rarity option \"{rarity}\" is invalid."
        rarity = Rarity.from_string(rarity)

        # 2) Query the database
        message_lines = []
        returnState = State.SUCCESS
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_type == SaleType.ITEM, f"{sku_entry.sku_id} is not an item, so it can't be a blueprint."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot add a note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            assert sku_entry.sale_state == SaleState.NO_RESERVATIONS, "Only sale items with no reservations can have their details edited."
            assert sku_entry.rarity != rarity, f"This rarity is already set as {rarity.print_name}."
            sku_entry.rarity = rarity

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message_lines.append(f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug.")
                returnState = State.WARNING

            if discord_message is not None:
                embed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                embed.SetNotes()
                embed.ForSale()
                await discord_message.edit(embed=embed.embed)
                message_lines.append(f"Updating the listing in {channel.mention}.")

        message.message = "\n".join(message_lines)
        await message.Print()
        return returnState

# ----------------------------------------------------------- #
#                     Note-related commands                   #
# ----------------------------------------------------------- #

    @shop.group()
    @commands.check(IsWhitelisted)
    @commands.check(CanAlterSale)
    @emojiResponse
    async def note(self, ctx):
        '''
        Add, remove, or edit notes for a sale item.
        '''
        assert ctx.invoked_subcommand is not None, "A valid subcommand is needed."
        return

    @note.command(name="add")
    @emojiResponse
    async def note_add(self, ctx, sku, *, note):
        '''
        Add a note to a sale item.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input - need to look at database first
        # 2) Query the database
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot add a note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            last_index = session.query(Note.index).filter_by(sku=sku_entry).order_by(Note.index.desc()).first()
            if last_index is None:
                last_index = 0
            else:
                last_index = last_index.index
            Note(sku=sku_entry, content=note, index=last_index + 1)  # Automatically added to db

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            # Update the embed with this new note
            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message.message = f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug."
                returnState = State.WARNING

            if discord_message is not None:
                skuEmbed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                skuEmbed.SetNotes()
                await discord_message.edit(embed=skuEmbed.embed)

            # 4) Update database
            # sku_entry.notes.append(new_note)

        return returnState

    @note.command(name='remove')
    @emojiResponse
    async def note_remove(self, ctx, sku, note_num):
        '''
        Remove a specific note from a sale item.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input - need to look at database first
        # 2) Query the database
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"{sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku.sku_id} is sold out."
            note = session.query(Note).filter_by(sku=sku_entry, index=note_num).first()
            assert note is not None, f"No note found for {sku} with index {note_num}."

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            session.delete(note)

            # Update the embed with this new note
            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message.message = f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug."
                returnState = State.WARNING

            if discord_message is not None:
                skuEmbed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                skuEmbed.SetNotes()
                await discord_message.edit(embed=skuEmbed.embed)

        # 4) Update database
        return returnState

    @note.command(name="edit")
    @emojiResponse
    async def note_edit(self, ctx, sku, note_num, *, note):
        '''
        Change the content of a note.
        '''
        message = Message(ctx)
        message.raw = True
        # 1) Parse input - need to look at database first
        # 2) Query the database
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert sku_entry is not None, f"{sku} is not in the shop."
            assert sku_entry.sale_state != SaleState.CANCELLED, f"Cannot edit this note, {sku.sku_id} has been cancelled by the seller."
            assert sku_entry.sale_state != SaleState.SOLD_OUT, f"Cannot edit this note, {sku.sku_id} is sold out."
            note_entry = session.query(Note).filter_by(sku=sku_entry, index=note_num).first()
            assert note is not None, f"No note found for {sku} with index {note_num}."

            note_entry.content = note

            # 3) Update discord
            returnState = State.SUCCESS
            channel = self.bot.get_channel(sku_entry.type.channel.discord_id)
            assert channel is not None, "Channel in the database no longer exists. This is a bug."

            # Update the embed with this new note
            discord_message = None
            try:
                discord_message = await channel.fetch_message(sku_entry.listing_discord_id)
            except Exception:
                message.message = f"This {sku_entry.type.sale_type.value} has no message to remove - this is a bug."
                returnState = State.WARNING

            if discord_message is not None:
                skuEmbed = SkuEmbed.from_message(discord_message, self.bot, sku_entry)
                skuEmbed.SetNotes()
                await discord_message.edit(embed=skuEmbed.embed)

        # 4) Update database
        return returnState

# ----------------------------------------------------------- #
#                        Search commands                      #
# ----------------------------------------------------------- #

    @shop.group()
    @commands.check(IsWhitelisted)
    @emojiResponse
    async def search(self, ctx):
        '''
        Search through the database using one of the keywords below.
        '''
        assert ctx.invoked_subcommand is not None, "A valid subcommand is needed."
        return

    @search.command(name="type")
    @emojiResponse
    async def search_type(self, ctx, type):
        '''
        Search for a specific animal/item currently for sale.
        '''
        message = Message(ctx)
        message.raw = False
        with session_scope(self.db) as session:
            type_entry = session.query(Type).filter_by(sub_name=type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
            assert type_entry is not None, f"Type \"{type}\" is not in the database for this guild."

            skus = session.query(Sku).filter_by(type=type_entry).filter(Sku.sale_state != SaleState.CANCELLED, Sku.quantity_remaining != 0).all()
            if len(skus) == 0:
                message.message = f"No {type}s are currently active on this server."
                message.raw = True
                await message.Print()
                return State.WARNING

            if type_entry.sale_type == SaleType.ANIMAL:
                mat = [['ID', 'Price', 'Type', 'Quantity', 'Level', 'Channel']]
                for sku in skus:
                    price = sku.price_per
                    if price == -1:
                        price = sku.versions[0].price_per
                    discord_channel = self.bot.get_channel(id=sku.type.channel.discord_id)
                    mat.append([sku.sku_id, price, sku.type.sub_name, sku.quantity_remaining, sku.level, discord_channel.name])
            else:
                mat = [['ID', 'Price', 'Type', 'Quantity', 'Rarity', 'Channel']]
                for sku in skus:
                    price = sku.price_per
                    if price == -1:
                        price = sku.versions[0].price_per
                    discord_channel = self.bot.get_channel(id=sku.type.channel.discord_id)
                    type_name = sku.type.sub_name
                    if sku.blueprint:
                        type_name += " BP"
                    mat.append([sku.sku_id, price, type_name, sku.quantity_remaining, sku.rarity.print_name, discord_channel.name])

            message.message = f"{type_entry.sub_name}'s for sale on this server:\n" + GetMatPrintString(mat)
        await message.Print()
        return State.SUCCESS

    @search.command(name="seller")
    @emojiResponse
    async def search_seller(self, ctx, member):
        '''
        Search all animals/items currently for sale by a specific seller.
        '''
        message = Message(ctx)
        message.raw = True
        # Get the member from the bot
        aP = ArgParser(ctx)
        aP.requiredPatterns = {
            "seller": ConvertArg(CIMember)
        }
        seller = await aP.Parse([member])
        seller = seller['seller']
        if isinstance(seller, discord.Member):
            self.Add_member_to_DB(ctx.guild, seller)
        with session_scope(self.db) as session:
            server_skus = session.query(Sku).filter(Sku.sale_state != SaleState.CANCELLED, Sku.quantity_remaining != 0).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).all()
            skus = []
            for sku in server_skus:
                if sku.seller.discord_id == seller.id:
                    skus.append(sku)
            if len(skus) == 0:
                message.message = f"{seller.display_name} currently has no active sales on this server."
                message.raw = True
                await message.Print()
                return State.WARNING

            sale_value = 0
            animal_mat = [['ID', 'Price', 'Level', 'Type', 'Quantity', 'Channel']]
            item_mat = [['ID', 'Price', 'Rarity', 'Type', 'Quantity', 'Channel']]
            for sku in skus:
                price = sku.price_per
                if price == -1:
                    price = sku.versions[0].price_per
                discord_channel = self.bot.get_channel(id=sku.type.channel.discord_id)
                type_name = sku.type.sub_name
                sale_value += sku.quantity_remaining * price
                if sku.type.sale_type == SaleType.ITEM:
                    if sku.blueprint:
                        type_name += " BP"
                    item_mat.append([sku.sku_id, price, sku.rarity.print_name, type_name, sku.quantity_remaining, discord_channel.name])
                else:
                    animal_mat.append([sku.sku_id, price, sku.level, sku.type.sub_name, sku.quantity_remaining, discord_channel.name])

        message_parts = [f"Current sales on this server posted by {seller.display_name}:"]
        if len(animal_mat) > 1:
            message_parts.append("Animal sales:\n```" + GetMatPrintString(animal_mat) + "```")
        if len(item_mat) > 1:
            message_parts.append("Item sales:\n```" + GetMatPrintString(item_mat) + "```")
        message_parts.append("Total value: " + str(sale_value) + "g")
        message.message = "\n".join(message_parts).replace("```\n", "```")
        await message.Print()
        return State.SUCCESS

    @search.command(name="reservations")
    @emojiResponse
    async def search_reservations(self, ctx, reserver):
        '''
        Search for all animals/items reserved by a specific member.
        '''
        message = Message(ctx)
        message.raw = True
        # Get the member from the bot
        aP = ArgParser(ctx)
        aP.requiredPatterns = {
            "reserver": ConvertArg(CIMember)
        }
        reserver = await aP.Parse([reserver])
        reserver = reserver['reserver']
        if isinstance(reserver, discord.Member):
            self.Add_member_to_DB(ctx.guild, reserver)
        with session_scope(self.db) as session:
            server_skus = session.query(Sku).filter(Sku.sale_state != SaleState.CANCELLED).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).all()
            reserver_entry = session.query(Member).filter_by(discord_id=reserver.id).one()
            skus = []
            for sku in server_skus:
                if reserver_entry in sku.ReservingMembers():
                    skus.append(sku)
            print(f"Total of {len(skus)} different sales have been reserved for {reserver_entry.username}.")

            total_owed = 0
            animal_mat = [['ID', 'Price', 'Level', 'Type', 'Quantity', 'Channel']]
            item_mat = [['ID', 'Price', 'Rarity', 'Type', 'Quantity', 'Channel']]
            for sku in skus:
                price = sku.price_per
                if price == -1:
                    price = sku.versions[0].price_per
                discord_channel = self.bot.get_channel(id=sku.type.channel.discord_id)
                quantity = reserver_entry.get_total_reservations(sku.id)
                if quantity == 0:
                    continue
                type_name = sku.type.sub_name
                if sku.type.sale_type == SaleType.ITEM:
                    if sku.blueprint:
                        type_name += " BP"
                    item_mat.append([sku.sku_id, price, sku.rarity.print_name, type_name, quantity, discord_channel.name])
                else:
                    animal_mat.append([sku.sku_id, price, sku.level, sku.type.sub_name, quantity, discord_channel.name])

                total_owed += quantity * price

            if len(animal_mat) == 1 and len(item_mat) == 1:
                message.message = f"{reserver.display_name} currently has no active reservations on this server."
                message.raw = True
                await message.Print()
                return State.WARNING

        message_parts = [f"Current reservations on this server for {reserver.display_name}:"]
        if len(animal_mat) > 1:
            message_parts.append("Animals:\n```" + GetMatPrintString(animal_mat) + "```")
        if len(item_mat) > 1:
            message_parts.append("Items:\n```" + GetMatPrintString(item_mat) + "```")
        message_parts.append("Total owed: " + str(total_owed) + "g")
        message.message = "\n".join(message_parts).replace("```\n", "```")
        await message.Print()
        return State.SUCCESS

    @search.command(name="purchases")
    @emojiResponse
    async def search_purchases(self, ctx, buyer):
        '''
        Search for all animals/items that a specific person has ever purchased.
        '''
        message = Message(ctx)
        message.raw = True
        # Get the member from the bot
        aP = ArgParser(ctx)
        aP.requiredPatterns = {
            "buyer": ConvertArg(CIMember)
        }
        buyer = await aP.Parse([buyer])
        buyer = buyer['buyer']
        if isinstance(buyer, discord.Member):
            self.Add_member_to_DB(ctx.guild, buyer)
        with session_scope(self.db) as session:
            buyer_entry = session.query(Member).filter_by(discord_id=buyer.id).one()
            skus = {}

            for p in buyer_entry.purchases:
                if p.sku.type.channel.guild.discord_id != ctx.guild.id:
                    continue
                if p.sku.sku_id not in skus:
                    skus[p.sku.sku_id] = [p.sku, 0]
                skus[p.sku.sku_id][1] += p.purchase_amount

            if len(skus.keys()) == 0:
                message.message = f"{buyer.display_name} currently has no purchases on this server."
                message.raw = True
                await message.Print()
                return State.WARNING

            total_paid = 0
            animal_mat = [['ID', 'Price', 'Level', 'Type', 'Quantity']]
            item_mat = [['ID', 'Price', 'Rarity', 'Type', 'Quantity']]
            for key, data in skus.items():
                sku = data[0]
                quantity = data[1]
                price = sku.price_per
                if price == -1:
                    price = sku.versions[0].price_per
                type_name = sku.type.sub_name
                if sku.type.sale_type == SaleType.ITEM:
                    if sku.blueprint:
                        type_name += " BP"
                    item_mat.append([sku.sku_id, price, sku.rarity.print_name, type_name, sku.quantity_remaining])
                else:
                    animal_mat.append([sku.sku_id, price, sku.level, sku.type.sub_name, sku.quantity_remaining])
                total_paid += quantity * price

        message_parts = [f"Purchases on this server for {buyer.display_name}:"]
        if len(animal_mat) > 1:
            message_parts.append("Animals:\n```" + GetMatPrintString(animal_mat) + "```")
        if len(item_mat) > 1:
            message_parts.append("Items:\n```" + GetMatPrintString(item_mat) + "```")
        message_parts.append("Total paid: " + str(total_paid) + "g")
        message.message = "\n".join(message_parts).replace("```\n", "```")

        await message.Print()
        return State.SUCCESS

# ----------------------------------------------------------- #
#                   Extra features/commands                   #
# ----------------------------------------------------------- #

    # @shop.command(usage='<sale id> <time limit> <starting bid> [silent] [')
    # @commands.check(IsWhitelisted)
    # @commands.check(CanAlterSale)
    # @commands.check(IsInBackend)
    # @emojiResponse
    # async def auction(self, ctx, sku, time_limit, starting_bid, channel: CIChannel, silent=False):
    #     '''
    #     Start an auction for the given animal - can only be used in the backend channel.

    #     Silent auctions can be chosen, in which case bids are done through DM.

    #     Required arguments:
    #     -------------------
    #     sku: ID of the sale item in this guild to be put up for auction.
    #     time_limit: Time for the auction to go on for, formatted like ###hr.
    #     starting_bid: Gold amount to start the auction for, formatted like #####g.
    #     channel: Add a channel mention to put the auction in a channel.

    #     Optional argument
    #     -----------------
    #     silent: Add "silent" to make the auction silent (bid through DMs)
    #     '''
    #     # 1) Accept and parse input from discord
    #     assert re.match(r'[0-9]{1,2}hr', time_limit, re.I), f"Time limit \"{time_limit}\" is invalid."
    #     time_limit = int(time_limit[:-2])
    #     assert re.match(r'[0-9]+\g', starting_bid, re.I), f"Starting bid {starting_bid} is invalid."
    #     starting_bid = int(starting_bid[:-1])
    #     assert isinstance(channel, discord.TextChannel), f"Channel {channel} does not exist - make it first."
    #     # Make sure that the bot has the right permissions in channel
    #     perms = channel.permissions_for(self.bot.user)
    #     assert perms.read_messages and perms.send_messages and perms.manage_messages, f"The bot needs to have the read, send, and manage message permissions for {channel.mention}."

    #     # 2) Query the database
    #     with session_scope(self.db) as session:
    #         sku_entry = session.query(Sku).filter_by(sku_id=sku).join(Type).join(Guild).filter_by(discord_id=ctx.guild.id).one_or_none()
    #         assert sku_entry is not None, "This sale item is not in the database."
    #         # Check if the item is for sale
    #         assert sku_entry.sale_state != SaleState.CANCELLED, f"{sku_entry.sku_id} has been cancelled by the seller."
    #         assert sku_entry.sale_state != SaleState.SOLD_OUT, f"{sku_entry.sku_id} is sold out - it can no longer be put up for auction."
    #         assert sku_entry.sale_state != SaleState.FULLY_RESERVED, f"{sku_entry.sku_id} is fully reserved, so it can't be put up for auction any more."

    #         # make a new listing with this embed and update the database's listing id
    #         oldChannel = self.bot.get_channel(sku_entry.type.channel.discord_id)
    #         message = await oldChannel.fetch_message(sku.listing_discord_id)
    #         embed = SkuEmbed.from_message(message, self.bot, sku_entry)
    #         listing = await channel.send(embed=embed.embed)
    #         sku_entry.listing_discord_id = listing.id

    #         # Since this sku is no longer in the channel that "type" says it should be in, we need to indicate that.
    #         sku_entry.IsInAuction = True
    #     # 3) Update discord
    #     # 4) Update database
    #     pass

    # @shop.command()
    # @commands.check(IsWhitelisted)
    # @emojiResponse
    # async def bid(self, ctx, sku, amount):
    #     '''
    #     Bid on the given auction.

    #     Arguments
    #     ---------

    #     sku: ID of the item for auction (e.g. ELE14)
    #     amount: Amount of gold you bid for the auction. Formatted like #####g.
    #     '''
    #     pass

# ----------------------------------------------------------- #
#                 Listeners - update the DB                   #
# ----------------------------------------------------------- #

    async def CancelAndNotify(self, message_id, guild_id):
        '''
        Cancel the sale with the listing id given, in the given guild. Then notify in the backend channel that it was cancelled.
        '''
        guild = self.bot.get_guild(guild_id)
        self.Add_member_to_DB(guild, DefaultMember)  # Going to be the person cancelling, since we don't know who did it.
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(listing_discord_id=message_id).join(Type).join(Guild).filter_by(discord_id=guild_id).one_or_none()
            if sku_entry is None:
                print(f"Deleted message {message_id} is not a sku.")
                return

            # If any reservations exist, notify in backend
            if sku_entry.type.guild.backend_channel_discord_id is not None:
                backend_channel = self.bot.get_channel(sku_entry.type.guild.backend_channel_discord_id)
                if sku_entry.sale_state in [SaleState.ONE_RESERVATION, SaleState.MULTIPLE_RESERVATIONS, SaleState.FULLY_RESERVED]:
                    for member in sku_entry.ReservingMembers():
                        discord_member = self.bot.get_user(member.discord_id)
                        number_reserved = member.get_total_reservations(sku_entry.id)
                        await PrintTo(backend_channel, f"{discord_member.mention} Your reservation for {number_reserved} {sku_entry.sku_id}s has been cancelled.", raw=True)
            # Cancel the sale in the db and alert in backend
            staff_entry = session.query(Member).filter_by(discord_id=DefaultMember.id).one()
            Cancellation(sku=sku_entry, staff=staff_entry, time=datetime.now())
            sku_entry.sale_state = SaleState.CANCELLED

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload):
        '''
        Remove any potential listings (in any state) from the DB.
        '''
        await self.CancelAndNotify(payload.message_id, payload.guild_id)
        return State.SUCCESS

    @commands.Cog.listener()
    async def on_raw_bulk_message_delete(self, payload):
        '''
        Remove any potential listings (in any state) from the DB.
        '''
        for message_id in payload.message_ids:
            await self.CancelAndNotify(message_id, payload.guild_id)
        return State.Success

    @commands.Cog.listener()
    async def on_raw_message_edit(self, payload):
        '''
        If it's a listing and the embed is missing, cancel the sale.
        '''
        if len(payload.data['embeds']) > 0:
            return State.SUCCESS

        guild_id = payload.data['guild_id']
        message_id = payload.message_id
        with session_scope(self.db) as session:
            sku_entry = session.query(Sku).filter_by(listing_discord_id=message_id).join(Type).join(Guild).filter_by(discord_id=guild_id).one_or_none()
            if sku_entry is None:
                print(f"Updated message {message_id} is not a sku.")
                return State.SUCCESS

        channel = self.bot.get_channel(payload.channel_id)
        message = await channel.fetch_message(message_id)
        await message.delete()
        await self.CancelAndNotify(message_id, guild_id)

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel):
        '''
        Check if it's a sale channel, and remove relevant entries from the DB.
        '''
        with session_scope(self.db) as session:
            guild = session.query(Guild).filter_by(discord_id=channel.guild.id).first()
            # Check if this was the shop category itself
            if channel.id == guild.category_discord_id:
                guild.category_discord_id = None
                # TODO: Cancel all sales for this guild?
                print("Removed the shop category")
                return State.SUCCESS

            # Check for backend channel
            if channel.id == guild.backend_channel_discord_id:
                guild.backend_channel_discord_id = None
                print("Removed the backend channel")
                return State.SUCCESS

            # Now check for all potential sales channels
            db_channel = session.query(Channel).filter_by(discord_id=channel.id).first()
            if db_channel is not None:
                db_channel.deleted = True
                print("A sales channel has been deleted")
            else:
                print("This channel was not in the db")
        return State.SUCCESS

    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        '''
        Update the last known username.
        '''
        if after.bot:  # Don't continuously update bot accounts...
            return State.SUCCESS
        self.Add_member_to_DB(after.guild, after, only_update=True)
        return State.SUCCESS

    @commands.Cog.listener()
    async def on_guild_remove(self, guild):
        '''
        Remove this guild and cancel all current DB items from this guild.
        '''
        with session_scope(self.db) as session:
            self.remove_guild_by_id(guild.id, session)
        return State.SUCCESS

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        '''
        Add this guild to the db, if it's not already there.
        '''
        with session_scope(self.db) as session:
            self.add_guild_by_id(guild.id, session)
        return State.SUCCESS

    @commands.Cog.listener()
    async def on_guild_role_delete(self, role):
        '''
        If this was the seller role, unset in options.
        '''
        guild = role.guild
        with session_scope(self.db) as session:
            guild_entry = session.query(Guild).filter_by(id=guild.id).first()
            if guild_entry is None:  # This server isn't whitelisted/set up
                return
            if guild_entry.seller_role_discord_id == role.id:
                guild_entry.seller_role_discord_id = None
        return

    @commands.Cog.listener()
    async def on_ready(self):
        # Check to make sure the database is still accurate
        self.check_db()

# ---------------------------------------------------- #
#                  Special Methods                     #
# ---------------------------------------------------- #

    @emojiResponse
    async def cog_command_error(self, ctx, error):
        message = Message(ctx)
        message.raw = True
        if isinstance(error, NoResultFound):
            message.message = "No entries found in the database."
        elif isinstance(error, commands.CommandInvokeError):
            if isinstance(error.original, AssertionError):
                await ctx.send_help(ctx.command)
            message.message = str(error.original)
            print("original: ", error.original)
        else:
            message.message = str(error)

        await message.Print()
        print(error)
        return State.FAILURE


def setup(bot):
    print("Loading New Discord Shop Cog")
    bot.add_cog(DiscordShopCog(bot))
    print("Discord Shop Cog successfully loaded.")
